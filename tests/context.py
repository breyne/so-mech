"""Context file for robustness.
Copied the idea from
https://kenreitz.org/essays/repository-structure-and-python
"""

import os
import sys

# The insertion below makes the script path-agnostic
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

#pylint: disable=wrong-import-position
#pylint: disable=unused-import
from somech import tensors
from somech import materials_core
from somech import materials_elasticity
from somech import materials_equivalent_stresses
from somech import materials_plasticity
from somech import visualization
from somech import kinematics
