""" somech.<module> unit testing

The Pytest library is used and can be run with
    pytest tests/<module>_test.py -v

Doctests  examples can be run using
    python3 somech/<module>.py -v
"""

import pytest
import numpy as np

from context import tensors as tsr
from context import materials_core as mc
from context import materials_equivalent_stresses as mes
from context import materials_plasticity as mp
Tensor = tsr.Tensor
EquivStress = mes.EquivalentStress

################################################################################

ZERO = Tensor([0, 0, 0, 0, 0, 0])
TENS = Tensor([1, 0, 0, 0, 0, 0])

MX_ISO = mp.PlasticIsotropic(
        mp.HardeningRule.hollomon(K=1e3, n=.5),
        #mp.HardeningRule.voce(),
        mes.EquivalentStress.mises())
MX_ISO.initial_state = dict(
    strain=ZERO,
    strain_cumul=0.,
    stress=ZERO)

MX_DISTO = mp.PlasticDistortional(
        mp.HardeningRule.hollomon(K=1e3, n=.5),
        #mp.HardeningRule.voce(),
        mes.EquivalentStress.yld2004())
MX_DISTO.initial_state = dict(
    strain=ZERO,
    strain_cumul=0.,
    stress=ZERO,
    added_int_var=MX_DISTO.equivalent_stress.parameters,
    )

LOAD = mc.Load().append(
        ('stress', TENS*500, 1, 100),
        ('stress', TENS*-500, 2, 100),
        )
LOAD_EPS = mc.Load().append(
        ('stress', TENS*500, 1, 1),
        ('strain_cumul', .1, 1, 10),
        ('strain', ZERO, 1, 1),
        ('stress', TENS*5, 1, 1),
        ('strain_cumul', .1, 2, 10),)

SOL = MX_ISO.load_with(LOAD)
SOL_EPS = MX_ISO.load_with(LOAD_EPS)

#from matplotlib import pyplot as plt
#p = SOL['strain_cumul']
#plt.plot(p, MX_ISO.hardening_rule(p), 'o')
#sig = np.vectorize(Tensor.norm)(SOL['stress'])
#plt.plot(p, sig)
#sig = np.vectorize(MX_ISO.equivalent_stress)(SOL['stress'])
#plt.plot(p, sig)
#plt.show()

################################################################################

#pylint: disable=no-self-use
class TestHardeningRule:
    """Methods execution and algorithmic limits"""
    def test_init(self):
        """Default inits"""
        mp.HardeningRule.hollomon(K=1e3, n=.5)
        mp.HardeningRule.ludwik(sig_y=140, K=1e3, n=.5)
        mp.HardeningRule.swift(K=10., eps_0=4, n=2)
        mp.HardeningRule.voce(sig_s=200, sig_y=150, eps_0=1e-3)
    def test_call(self):
        """Function and inverse"""
        for my_rule in [
                mp.HardeningRule.hollomon(),
                mp.HardeningRule.ludwik(),
                mp.HardeningRule.swift(),
                mp.HardeningRule.hocket_sherby(),
                mp.HardeningRule.voce()]:
            assert abs(my_rule.inverse(my_rule(0))) == 0
            assert abs(my_rule.inverse(my_rule(1))-1) < 1e-12
    def test_derive(self):
        """Values should be properly checked, I mean not like that :/"""
        for my_rule in [
                mp.HardeningRule.hollomon(),
                mp.HardeningRule.ludwik(),
                mp.HardeningRule.swift(),
                mp.HardeningRule.hocket_sherby(),
                mp.HardeningRule.voce()]:
            assert not np.isnan(my_rule.derivative(0))

class TestPlaticIsotropic:
    """Methods execution and algorithmic limits"""
    def test_init_default(self):
        """Init-it"""
        mp.PlasticIsotropic(
            mp.HardeningRule.voce(),
            mes.EquivalentStress.mises())
    def test_init_with_error(self):
        """Not passing any callable"""
        with pytest.raises(Exception) as excinfo:
            mp.PlasticIsotropic('not a callable', 'not a callable', 'not a callable')
        assert "should be callable" in str(excinfo.value)
    def test_solve_stress(self):
        """Solve it"""
        sol = MX_ISO.load_with(LOAD)
        # Ensure all solutions are effectively computed
        for inc in sol:
            assert np.prod(MX_ISO._is_set_state(inc)) == 1#pylint: disable=protected-access
    def test_solve_strain(self):
        """Solve it"""
        sol = MX_ISO.load_with(LOAD_EPS)
        # Ensure all solutions are effectively computed
        for inc in sol:
            assert np.prod(MX_ISO._is_set_state(inc)) == 1#pylint: disable=protected-access

class TestPlaticDistortional:
    """Methods execution and algorithmic limits"""
    def test_init_default(self):
        """Init-it"""
        mp.PlasticDistortional(
            mp.HardeningRule.voce(),
            mes.EquivalentStress.hah2020())
    def test_init_with_error(self):
        """Not passing any callable"""
        with pytest.raises(Exception) as excinfo:
            mp.PlasticDistortional('not a callable', 'not a callable', 'not a callable')
        assert "should be callable" in str(excinfo.value)
    def test_solve_stress(self):
        """Solve it"""
        sol = MX_DISTO.load_with(LOAD)
        # Ensure all solutions are effectively computed
        for inc in sol:
            assert np.prod(MX_DISTO._is_set_state(inc)) == 1#pylint: disable=protected-access
    def test_solve_strain(self):
        """Solve it"""
        sol = MX_DISTO.load_with(LOAD_EPS)
        # Ensure all solutions are effectively computed
        for inc in sol:
            assert np.prod(MX_DISTO._is_set_state(inc)) == 1#pylint: disable=protected-access
