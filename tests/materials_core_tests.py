""" somech.<module> unit testing

The Pytest library is used and can be run with
    pytest tests/<module>_test.py -v

Doctests  examples can be run using
    python3 somech/<module>.py -v
"""

import pytest
import numpy as np
from context import tensors as tsr
from context import materials_core as mc
Tensor = tsr.Tensor
ParFun = mc.ParametricFunction

################################################################################

DT = np.dtype([
        ('temperature', float),
        ('stress', Tensor)
        ])

TENS1 = Tensor([1, 0, 0, 0, 0, 0])
TENS2 = Tensor([0, 1, 0, 0, 0, 0])
TENS3 = Tensor([0, 0, 1, 0, 0, 0])

#pylint: disable=bad-continuation
LOAD = mc.Load().append(
    ('stress', TENS1, 1, 1),
    ('stress', TENS2, 1, 1),
    ('stress', TENS3, 1, 1))

DI = mc.ExampleModel()

def linfun(arg, **p):
    """Affine function"""
    p.setdefault('a', 1.)
    p.setdefault('b', 0.)
    return p['a'] * arg + p['b']

LINEAR = mc.ParametricFunction(linfun)

################################################################################

#pylint: disable=no-self-use
class TestConstitutiveModel():
    """ConstitutiveModel class methods"""

    def test_constructor_abstract(self):
        """Try instantiating abstract class"""
        with pytest.raises(Exception) as excinfo:
            mc.ConstitutiveModel()#pylint: disable=abstract-class-instantiated
        assert 'instantiate abstract class' in str(excinfo.value)
        return excinfo

    def test_atribute_dim(self):
        """Try setting and getting"""
        # Default value
        assert DI.dim == DI._dim == 3 #pylint: disable=protected-access
        # Change with wrong value
        with pytest.raises(Exception) as excinfo:
            DI.dim = 4
        assert '`dim` should be in' in str(excinfo.value)
        # Change with fine value
        DI.dim = 2
        assert DI.dim == DI._dim == 2 #pylint: disable=protected-access

    def test_initial_state(self):
        """Try setting and getting"""
        # Default value and getter
        assert DI.initial_state is None
        # Change with wrong type
        with pytest.raises(Exception) as excinfo:
            DI.initial_state = 4
        assert 'object has no attribute' in str(excinfo.value)
        # Change with not enough args
        with pytest.raises(Exception) as excinfo:
            DI.initial_state = dict(temperature=373.)
        assert 'Nothing should be empty after defining the initial state' in str(excinfo.value)
        # Change with fine value
        DI.initial_state = dict(temperature=373., stress=TENS1*0)
        assert DI.initial_state['temperature'][0] == 373.
        assert DI.initial_state['stress'][0] == TENS1*0

    def test_load_with(self):
        """Just run and see"""
        DI.initial_state = dict(temperature=373., stress=TENS1*0)
        DI.load_with(LOAD)

class TestLoad():
    """docstring"""
    def test_step_count(self):
        """docstring"""
        assert LOAD.count_steps() == 3
    def test_increment_count(self):
        """docstring"""
        assert LOAD.total_nb_of_inc() == 3
    def test_increment_generate(self):
        """docstring"""
        for incr in LOAD.generate_increments():
            assert len(incr) == 4

def test_parametric_function():
    """Docstring"""
    assert LINEAR(0.) == 0
    LINEAR.parameters_update(b=5.)
    assert LINEAR(0.) == 5
