""" somech.<module> unit testing

The Pytest library is used and can be run with
    pytest tests/<module>_test.py -v

Doctests  examples can be run using
    python3 somech/<module>.py -v
"""

import pytest
import numpy as np
#from matplotlib import pyplot as plt
from context import tensors as tsr
from context import materials_core as mc
from context import materials_elasticity as me
Tensor = tsr.Tensor

################################################################################

ALU = me.LinearElastic(E=70000, nu=.33)

ZERO = Tensor([0, 0, 0, 0, 0, 0])
SIG_TENS = Tensor([1, 0, 0, 0, 0, 0])
EPS_TENS = Tensor([1, -.30, -.30, 0, 0, 0])

ALU.initial_state = dict(
    strain=ZERO,
    stress=ZERO)

LOAD = mc.Load().append(
        ('stress', SIG_TENS*70, 1, 5),
        ('strain', EPS_TENS*(-1e-3), 1, 5),
        )

SOL = ALU.load_with(LOAD)

# That's note taking: I think vecotize is the optimal way to apply operations
SIG = np.vectorize(Tensor.norm)(SOL['stress'])
EPS = np.vectorize(Tensor.norm)(SOL['stress'])
#plt.plot(EPS, SIG, '-x')
#plt.show()

################################################################################

#pylint: disable=no-self-use
class TestLinearElastic:
    """somech.materials_elasticity.LinearElastic"""
    def test_init_errors(self):
        """generate implemented exceptions"""
        with pytest.raises(Exception) as excinfo:
            me.LinearElastic()
        assert '0 parameters. This is not supported.' in str(excinfo.value)
        with pytest.raises(Exception) as excinfo:
            me.LinearElastic(a=1, b=2)
        assert "expected parameter keys are" in str(excinfo.value)
    def test_tangent_operators(self):
        """make sure stifness and complicance make identity"""
        ide_1 = ALU.stiffness()**ALU.compliance()
        ide_2 = Tensor.projector('ide', 3, order=4)
        assert Tensor.norm(ide_1 - ide_2) < 1e-15
