""" somech.<module> unit testing

The Pytest library is used and can be run with
    pytest tests/<module>_test.py -v

Doctests  examples can be run using
    python3 somech/<module>.py -v
"""

#import time
import pytest
import numpy as np
import context as co

def foo_f(t):
    return co.tensors.Tensor(
            [[1. ,t,0],
            [0. ,1.,0],
            [0. ,0.,1],
            ], 3)
def foo_l(t):
   return co.tensors.Tensor(
            [[0. ,1.,0],
            [0. ,0.,0],
            [0. ,0.,0],
            ], 3)

LDF = co.kinematics.LocalDeformation(velocity_gradient=foo_l,)
LDL = co.kinematics.LocalDeformation(deformation_gradient=foo_f,)

def test_constructor():
    """Regular LocalDef constructors"""
    co.kinematics.LocalDeformation(velocity_gradient=foo_l,)
    co.kinematics.LocalDeformation(deformation_gradient=foo_f,)

def test_constructor_errors():
    """Regular LocalDef constructor errors"""
    with pytest.raises(Exception) as excinfo:
        co.kinematics.LocalDeformation()
    assert "Must specify" in str(excinfo.value)
    with pytest.raises(Exception) as excinfo:
        co.kinematics.LocalDeformation(
                velocity_gradient=foo_l,
                deformation_gradient=foo_f)
    assert "Don't give both F and l" in str(excinfo.value)
    with pytest.raises(Exception) as excinfo:
        co.kinematics.LocalDeformation(velocity_gradient=foo_l,d_t='abc')
    assert "could not convert string" in str(excinfo.value)

def test_integration_dydx():
    """ Checking that the imposed and evaluated always match"""
    dgam_l=np.array([x.values[0,1] for x in LDL.velocity_gradient(.1, generator=True)])
    dgam_f=np.array([x.values[0,1] for x in LDF.velocity_gradient(.1, generator=True)])
    assert sum((dgam_l-dgam_f)**2.) < 1e-20
    gam_l=np.array([x.values[0,1] for x in LDL.deformation_gradient(.1, generator=True)])
    gam_f=np.array([x.values[0,1] for x in LDF.deformation_gradient(.1, generator=True)])
    assert sum((gam_l-gam_f)**2.) < 1e-20


## Quick tests for how long the Tensor stuff takes
#LA = np.array([[0. ,1.,0], [0. ,0.,0], [0. ,0.,0]])
#LT = co.tensors.Tensor(LA, 3)
#
#t1=time.process_time()
#for tt in range(10000):
#    a=LA.dot(LA)
#t2=time.process_time()
#print('LA.dot(LA)\t'+str((t2-t1)*1e3))
#
#t1=time.process_time()
#for tt in range(10000):
#    a=LT*LT
#t2=time.process_time()
#print('LT*LT     \t'+str((t2-t1)*1e3))
