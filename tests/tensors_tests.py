""" somech.tensors unit testing

The Pytest library is used and can be run with
    pytest tests/tensors_test.py -v

Doctests  examples can be run using
    python3 somech/tensors.py -v

"""

import pytest
import numpy as np
from context import tensors as tsr
Tensor = tsr.Tensor

# For later use here is a deformation gradient:
F = Tensor([
    [1.0, 0.3, 0.0],
    [0.0, 1.0, 0.1],
    [0.9, 0.0, 1.0]], 3)
# And right Cauchy--Green tensor
C = (F.T*F).in_tens_basis()

# Helper function for random generation
def rnd_arr(size, order):
    """Generate random arrays of given size and order"""
    scale = 100.
    arr = np.random.rand(size)
    for _ in range(order-1):
        arr = np.tensordot(arr, np.random.rand(size), axes=0)
    return scale * arr


# The naming convention below is:
# D<dimension>O<order><V(ect)T(ens)>

D2O1V = Tensor(rnd_arr(2, 1), 2)
D2O2V = Tensor(rnd_arr(2, 2), 2)
D2O2T = Tensor(rnd_arr(3, 1), 2)
#D2O4V = Tensor(rnd_arr(2, 4), 2)# not supported
D2O4T = Tensor(rnd_arr(3, 2), 2)

D3O1V = Tensor(rnd_arr(3, 1), 3)
D3O2V = Tensor(rnd_arr(3, 2), 3)
D3O2T = Tensor(rnd_arr(6, 1), 3)
#D3O4V = Tensor(rnd_arr(2, 4), 3)# not supported
D3O4T = Tensor(rnd_arr(6, 2), 3)

# Lists
ALL = [D2O1V, D2O2V, D2O2T, D2O4T, D3O1V, D3O2V, D3O2T, D3O4T]
INNER1_SELF_COMPATIBLE = [D2O1V, D2O2V, D2O2T, D3O1V, D3O2V, D3O2T]
INNER2_SELF_COMPATIBLE = [D2O2V, D2O2T, D2O4T, D3O2V, D3O2T, D3O4T]

TOLERANCE = 1E-12

#pylint: disable=no-self-use
class TestConstructor:
    """Default and copy Tensor constructors

    Methods
    -------

    default :
        - normal uses

    copy :
        - normal uses

    errors :
        - wrong val type
        - no val passed
        - wrong array size
        - missing dim when needed
        - wrong dim when needed

    warning :
        - wrong dim when not needed

    """
    def test_default(self):
        """Regular cases with lists or ndarrays."""
        Tensor([1, 1])
        Tensor([[1, 1], [1, 1]])

    def test_copy(self):
        """Regular cases with a Tensor instance."""
        vector1 = Tensor([1, 1])
        vector2 = Tensor(vector1)
        # The instances point to different memory slots
        assert vector1 is not vector2
        assert vector1 == vector2
        # All their attributes point to different memory slots
        for attr in vector1.__dict__:
            assert id(vector1.__dict__[attr]) is not id(vector2.__dict__[attr])

    def test_error_no_arg(self):
        """Nothing is passed to instantiation"""
        with pytest.raises(Exception) as excinfo:
            Tensor() #pylint: disable=no-value-for-parameter
        assert "required positional argument" in str(excinfo.value)

    def test_error_wrong_arg_type(self):
        """Wrong type is passed to instantiation"""
        with pytest.raises(Exception) as excinfo:
            Tensor('a')
        assert "expecting an instance of Tensor or" in str(excinfo.value)

    def test_error_wrong_size(self):
        """An array is given with an unsupported shape"""
        with pytest.raises(Exception) as excinfo:
            Tensor([1])
        assert "unsupported size" in str(excinfo.value)

    def test_error_missing_dim(self):
        """No dimension is provided when it was necessary"""
        with pytest.raises(Exception) as excinfo:
            Tensor([1, 1, 1])
        assert "dimension" in str(excinfo.value)

    def test_warning_different_dims(self):
        """A wrong dimension is provided when it was not necessary"""
        with pytest.warns(Warning) as record:
            Tensor([1, 1], 6)
            Tensor([[1, 2], [5, 6]], 4)
        assert len(record) == 2
        assert "You provided" in str(record[0].message)
        assert "You provided" in str(record[1].message)

class TestOperators:
    """ Operators metods

    Methods
    -------

    """


    def test_doubles(self):
        """TODO"""
        for tens in ALL:
            assert tens + tens == 2*tens == +tens*2 == -(-tens-tens)

    def test_inner1(self):
        """All possible simple contractions are compared to their norm or transpose."""
        # Dimension 2
        assert 1 - D2O1V * D2O1V / (D2O1V.norm()**2.) < TOLERANCE
        assert D2O1V * D2O2V == D2O2V.T * D2O1V
        assert D2O1V * D2O2T == D2O2T.T * D2O1V
        assert (D2O2V * D2O2T).T == D2O2T.T * D2O2V.T
        # Dimension 3
        assert 1 - D3O1V * D3O1V / D3O1V.norm()**2 < TOLERANCE
        assert D3O1V * D3O2V == D3O2V.T * D3O1V
        assert D3O1V * D3O2T == D3O2T.T * D3O1V
        assert (D3O2V * D3O2T).T == D3O2T.T * D3O2V.T

    def test_inner2(self):
        """All possible double contractions are compared to their norm or transpose."""
        # Dimension 2
        assert abs(1. - (D2O2V ** D2O2V) / (D2O2V.norm()**2)) < TOLERANCE
        assert abs(1. - (D2O2T ** D2O2T) / (D2O2T.norm()**2)) < TOLERANCE
        assert D2O2V.sym ** D2O2T == D2O2V.sym ** D2O2T
        #assert (D2O2V ** D2O4T).T == D2O4T.T ** D2O2V.T
        # Dimension 3
        assert abs(1. - (D3O2V ** D3O2V) / (D3O2V.norm()**2)) < TOLERANCE
        assert abs(1. - (D3O2T ** D3O2T) / (D3O2T.norm()**2)) < TOLERANCE
        assert D3O2V.sym ** D3O2T == D3O2V.sym ** D3O2T
        assert (D3O2V.sym ** D3O4T).T == D3O4T.T ** D3O2V.sym.T

    def test_outer(self):
        """Try all possible outer products and compare with expected norm."""
        for tens in [D2O1V, D3O1V, D2O2T, D3O2T]:
            assert abs(1-(tens % tens).norm() / tens.norm()**2) < TOLERANCE

    def test_cross(self):
        """Make sure cross products yield orthogonal vectors"""
        tens1 = Tensor(D3O1V).normed
        tens2 = (D3O2V * tens1).normed
        cross = tens1 ^ tens2
        assert cross*tens1 < TOLERANCE
        assert cross*tens2 < TOLERANCE

    def test_inverse(self):
        """Make sure the product with inverse is close to Identity"""
        for tens in INNER2_SELF_COMPATIBLE:
            tens = tens.sym.normed # better invertibility
            un_ = Tensor.projector('unity', tens.dim, order=tens.ord)
            tens += un_  # better invertibility
            if tens.ord == 2:
                assert abs((tens * ~tens - un_).norm()) < TOLERANCE
                assert abs((~tens * tens - un_).norm()) < TOLERANCE
            else:# The behavior is so bad with randomly generated tensors
                # assert abs((tens ** ~tens- un_).norm()) < TOLERANCE
                # assert abs((~tens ** tens- un_).norm()) < TOLERANCE
                pass

class TestMethods:
    """ Non-operators methods

    Methods
    -------

    get_attributes
        - values
        - dim
        - ord

    """

    def test_print(self):
        """This is checked by doctest"""

    def test_projectors(self):
        """Make sure they make sense"""
        for dim in [2, 3]:
            i_2 = Tensor.projector('ide', dim, order=2)
            i_4 = Tensor.projector('ide', dim, order=4)
            p_d = Tensor.projector('dev', dim, order=4)
            p_h = Tensor.projector('hyd', dim, order=4)
            # Verify orthogonality
            assert (p_d**p_h).norm() < TOLERANCE
            list_sig = [1, 2, 0] if dim == 2 else [1, 2, 3, 0, 0, 0]
            sig_ = Tensor(np.array(list_sig), dim)
            # Verify identities
            assert sig_*i_2 == sig_**i_4 == sig_
            # Verify hyd and dev methods
            assert sig_.dev == sig_ ** p_d
            assert sig_.hyd == sig_ ** p_h

    def test_base_change(self):
        """Make sure symmetric tensors are unchanged after their basis is modified"""
        for tens in [D2O2V, D3O2V]:
            s_1 = tens.sym
            s_2 = Tensor(s_1)
            s_2.tensorize_basis()
            assert abs(1-(s_1**s_2)/s_1.norm()**2) < TOLERANCE
            s_2.vectorize_basis()
            assert abs(1-(s_1**s_2)/s_1.norm()**2) < TOLERANCE

    def test_invariants(self):
        """We just try to compute them"""
        assert F.det() > 0.
        assert F.trace() == 3.
        assert C.invariant('J1') == (F.T*F).invariant('J1')
        assert C.invariant('J2') == (F.T*F).invariant('J2')
        assert C.invariant('J3') == (F.T*F).invariant('J3')
        assert C.invariant('I1') == (F.T*F).invariant('I1')
        assert C.invariant('I2') == (F.T*F).invariant('I2')
        assert C.invariant('I3') == (F.T*F).invariant('I3')

    def test_eigenvalue_problem(self):
        """Confront tensors to their eigen decomposition"""
        for tens in  [D2O2V, D2O2T, D3O2V, D3O2T]:
            # Improve conditioning
            tens = tens.sym

            # Compute the eigenstuff
            vals, vecs = tens.decomposition_eigen()
            vals = np.array(vals)
            vecs = np.array(vecs)

            # Recreate the tensor
            # The cool thing is ndarrays of tensors can still use their operations
            # So \sum_i ai * vi x vi reads
            new_tens = ((vecs%vecs)*vals).sum()
            assert abs((new_tens-tens).norm()/tens.norm()) < TOLERANCE

    def test_tensorlog(self):
        """Confront tensors to their log"""
        raise NotImplementedError('TODO')

    def test_polar_decomposition(self):
        """Confront tensors to their polar decompositions"""
        for tens in  [D2O2V, D3O2V]:
            rot_u, dil_u = tens.decomposition_polar('right')
            rot_v, dil_v = tens.decomposition_polar('left')
            assert abs((rot_u - rot_v).norm()) < TOLERANCE
            assert abs((dil_v*rot_v - tens).norm()/tens.norm()) < TOLERANCE
            assert abs((rot_u*dil_u - tens).norm()/tens.norm()) < TOLERANCE

    def test_angle2deviator(self):
        """See if that makes sense"""
        assert abs(np.sin(Tensor.eigendeviator2polar((1,0,-1,))[1])) < 1e-12
        assert abs(np.cos(Tensor.eigendeviator2polar((0,1,0,))[1])) < 1e-12
