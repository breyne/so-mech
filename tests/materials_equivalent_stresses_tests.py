""" somech.<module> unit testing

The Pytest library is used and can be run with
    pytest tests/<module>_test.py -v

Doctests  examples can be run using
    python3 somech/<module>.py -v
"""

import pytest
import numpy as np
from context import tensors as tsr
from context import materials_equivalent_stresses as mes
Tensor = tsr.Tensor
EquivStress = mes.EquivalentStress

################################################################################

TRE = EquivStress.tresca()
MIS = EquivStress.mises()
HAH = EquivStress.hah2020()

TEN = Tensor([1, -.5, -.5, 0, 0, 0])

## Below is a note on how to plot the equivalent scalar in the Pi-plane
#from matplotlib import pyplot as plt
#RAD, ANG = TRE.polar_curve(1, 250)
#ax = plt.subplot(111, projection='polar')
#ax.plot(ANG, RAD, 'r-+')
#plt.show()

################################################################################

#pylint: disable=no-self-use
class TestEquivalentStress():
    """Docstring"""
    def test_initialization(self):
        """Test some inputs for function and derivative"""
        stupid_model = EquivStress(lambda sig: 0.)
        assert stupid_model(TEN) == stupid_model(2*TEN) == 0.
        assert stupid_model.derivative(TEN) == 0.*TEN
        stupid_model.function_derivative = lambda x: x*66.
        assert stupid_model.derivative(TEN) == 66.*TEN
    def test_initialization_errors(self):
        """Test some inputs for function and derivative"""
        with pytest.raises(Exception) as excinfo:
            EquivStress('not a callable')
        assert "expecting a callable" in str(excinfo.value)
    def test_mises(self):
        """VonMises builtin object"""
        assert MIS(TEN) == TEN.dev.norm()*np.sqrt(3./2.)
        assert MIS.derivative(TEN) == TEN.dev.normed * np.sqrt(3./2.)
    def test_tresca(self):
        """VonMises builtin object"""
        assert TRE(TEN) > 0.
