""" somech.<module> unit testing

The Pytest library is used and can be run with
    pytest tests/<module>_test.py -v

Doctests  examples can be run using
    python3 somech/<module>.py -v
"""
import plotly.graph_objects as go
from context import visualization as visu
from context import tensors as tsr
from context import materials_equivalent_stresses as mes
Tensor = tsr.Tensor

#fig = go.Figure() # or any Plotly Express function e.g. px.bar(...)
# fig.add_trace( ... )
# fig.update_layout( ... )

TEN = Tensor([1, -1, -1, 0, 0, 0]).dev
HAH = mes.EquivalentStress.hah2020()
HAH.parameters_update(h=tsr.Tensor([1, 1, 0, 0, 0, 0]).dev.normed)
EQS = mes.EquivalentStress.hill48()

FIG = go.Figure()
CANVAS = visu.Graph('deviatoric plane')
CANVAS = visu.Graph('2d plane')
FIG.add_trace(CANVAS.trace(
    TEN, dict(type='line'),
    line=dict(color='black'),
    showlegend=False,
    mode='markers',
    ))
FIG.add_trace(CANVAS.trace(
    HAH,
    name="HAH",
    line=dict(color='black'),
    ))

#pylint: disable=no-self-use
class TestTraces:
    """Tensors and EqStresses is different canvas"""
    def test_piplane(self):
        """Deviatoric plane canvas"""
        fig = go.Figure()
        canvas = visu.Graph('deviatoric plane')
        fig.add_trace(canvas.trace(TEN, dict(type='line'),))
        fig.add_trace(canvas.trace(HAH))
    def test_ps2d(self):
        """2d plane state canvas"""
        fig = go.Figure()
        canvas = visu.Graph('2d plane')
        fig.add_trace(canvas.trace(TEN, dict(type='other')))
        fig.add_trace(canvas.trace(HAH))

#pylint: disable=too-few-public-methods
class TestTikz:
    """Tikz export functions"""
    def test_piplane(self):
        """Deviatoric plane canvas"""
        canvas = visu.Graph('deviatoric plane')
        canvas.tikz_export(TEN, file_name=None)
        canvas.tikz_export(
            EQS,
            options=dict(
                amount=100,
                value=10.,
                style='black, dashed',
            )
        )
