# `somech`: Simpler Operations in MECHanics

*utilities for local continuum mechanics computation and representation*

The initial issue is that it often takes too long to run simple tests when an idea pops up. Several causes are as many motivations; several examples are given below.

- When manipulating tensors, take a stress state (2nd order tensor) for example. One must choose between vectorial bases represented in 3 by 3 arrays; or symmetric 2nd order tensorial bases represented in 6 by 1 arrays. Operations between the two cannot be run simply.
  The `Tensor` class runs the appropriate operations in the background.
- Testing or altering an existing constitutive model can take hundreds of lines from solving to plotting.
  The `materials` library tries to make definition, solving and plotting shorter.

## tensors

When manipulating tensors, take a stress state (2nd order tensor) for example. One must choose between vectorial bases represented in 3 by 3 arrays; or symmetric 2nd order tensorial bases represented in 6 by 1 arrays. Operations between the two cannot be run simply.
The Tensor class runs the appropriate operations in the background.

```python
import somech as so
# Define a simple shear deformation gradient
F = so.Tensor([
	[1, 0, 0],
	[0, 1, 0],
	[.5, 0, 1]
	], 3) # the last argument is the dimension
# Compute the associated right Cauchy-Green tensor
C = F.T*F; print(C)
```
Output:
```python
[[1.25 0.   0.5 ]
 [0.   1.   0.  ]
 [0.5  0.   1.  ]]_(3D,O2)
```
```python
# C is inherently symmetric so in better be stored in a `2nd order symmetric tensorial` basis
C.tensorize_basis(); print(C)
```
Output:
```python
[1.25       1.         1.         0.         0.70710678 0.        ]_(3D,O2)

```

```python
# Any tensorial operation will work regardless of the storage type:
(~F.T)*C*(~F)
[[1. 0. 0.]
 [0. 1. 0.]
 [0. 0. 1.]]_(3D,O2)
```
In the operation above, attribute `T` is the transpose, operator `~` returns the inverse, and `*` the simple contraction (also returned by `so.Tensor.prod_inner1(lhs, rhs)`.
In the same logic, `**` returns the double contraction of two tensors (also returned by `so.Tensor.prod_inner2(lhs, rhs)`.

```python
# For more information
import somech as so
help(so.Tensor)
```

And check out `examples/tensors_play_with_shear.py`.

## materials

`so.EquivalentStress`, `so.HardeningRule`, `so.ConstitutiveModel`, 

## vizualization

`so.Graph`