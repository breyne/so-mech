r""" TODO: /!!!\ This is unfinished and not very clean
We seek the relationship between an angle in the physical space
and the same angle in the deviatoric space.
"""
import numpy as np
import plotly.graph_objects as go
import somech as so

def rot(ang_deg):#pylint: disable=invalid-name
    """Rotation matrix"""
    cos = np.cos(ang_deg*np.pi/180.)
    sin = np.sin(ang_deg*np.pi/180.)
    return so.Tensor([
        [+cos, -sin, 0.],
        [+sin, +cos, 0.],
        [  0.,   0., 1.]], 3)

SIG = so.Tensor([1,0,0,0,0,0], 3)
SIG_ = lambda ang: rot(ang).T*SIG*rot(ang)
SIG__ = lambda ang: rot(ang)*SIG*rot(ang).T

S = SIG.dev
S_ = lambda ang: SIG_(ang).dev

CHI = lambda ang: np.arccos(S.normed**S_(ang).normed)/np.pi*180

ARR_THE = np.arange(181)

ARR_CHI = np.vectorize(CHI)(ARR_THE)


FIG = go.Figure()
#FIG.add_trace(go.Scatter(x=ARR_THE, y=ARR_CHI))
FIG.add_trace(go.Scatter(x=ARR_THE, y=np.cos(ARR_CHI/180*np.pi)))
FIG.add_trace(go.Scatter(x=ARR_THE, y=np.sin(ARR_CHI/180*np.pi)))
#FIG.add_trace(go.Scatter(x=np.cos(ARR_THE/180*np.pi), y=np.cos(ARR_CHI/180*np.pi)))
#FIG.add_trace(go.Scatter(x=np.sin(ARR_THE/180*np.pi), y=np.sin(ARR_CHI/180*np.pi)))
FIG.show()
