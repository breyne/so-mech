#!/bin/bash

mkdir -p latex 

echo "%
% arara: pdflatex: { shell: yes, synctex: yes }
\documentclass{minimal}

\usepackage{pgfplots}
\pgfplotsset{compat=1.5}
\usepgfplotslibrary{polar}

\begin{document}
\begin{tikzpicture}
 \begin{polaraxis}[
  xtick={30, 90, 150, 210, 270, 330},
  ytick=\empty,
  xticklabels={{}, \$s_2\$, {}, \$s_3\$, {}, \$s_1\$},
 ]
 \pgfplotsset{eqs/.style={draw=red}}
" > latex/main.tex

python3 tikz_hah_crossloading.py | grep addplot >> latex/main.tex

echo "%
 \end{polaraxis}
\end{tikzpicture}
\end{document}
" >> latex/main.tex

clear
cat latex/main.tex

cd latex
arara main.tex
xdg-open main.pdf
