"""
This script is an example of definition, computation and visualizatioin using the hah2020 model.
We use the parameters identified for DP780 in Barlat et al. (2020).
"""

import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import somech as so

######################################################################
# 1) INSTANTIATING
######################################################################

# (i) Material definition --------------------------------------------

# Instantiate the material_plastic_distortional object
MX = so.PlasticDistortional(
        # First argument: a hardening rule instance
        so.HardeningRule.hocket_sherby(
            sig_y=425.39,
            sig_s=1257.3,
            eps_eta=1./3.2862,
            eta=.4443
        ),
        # Second argument: an equivalent stress instance
        so.EquivalentStress.hah2020(**{
            # Values from Barlat2020 for DP780
            'k_1': 58.99, 'k_2': 86.81, 'k_3': .2696, 'k_4': .8072, 'k_5': 9.535,
            'xi_S': 2., 'C': .6158, 'k_C': 137.6, 'k_C^p': 90.10,
            # Recommended values from Barlat2020 (general purpose)
            'q': 3., 'p': 3., 'k': 150., 'k^p': 75., 'xi_R': 8., 'xi_B': 1.5,
            'k_S': 150., 'xi_C': 4., 'xi_C^p': 1., 'k_L': 60., 'xi_L': .5, 'xi_L^p': .25
        }),
)

# HAH2020 is a heavy model that calls (hence stores) the hardening rule
# We can only assign it after the instantiation of MX because it refers to itself
MX.equivalent_stress.parameters_update(
        hardrule=MX.hardening_rule,)

# (ii) Loading definition --------------------------------------------

# Define some tensorial directions to re-use later
ZERO = so.Tensor([0, 0, 0, 0, 0, 0])
TENS1 = so.Tensor([1, 0, 0, 0, 0, 0])
TENS2 = so.Tensor([0, 1, 0, 0, 0, 0])
TENSS = so.Tensor([1, 0, -1, 0, 0, 0]).normed

# We must define the initial state, that is affect a first value to each of the stored variables.
MX.initial_state = dict(
    strain=ZERO,
    strain_cumul=0.,
    stress=ZERO,
    added_int_var=MX.equivalent_stress.parameters,
)

# so.Load() instantiates an empty load object while
# so.Load().append() adds lines to that load.
# Each line is composed of
# - the variable name (see MX.var_types)
# - the total incremented value
# - the time it takes
# - the number of divisions of the increment

## The load instance below is a double reverse loading
#LOAD = so.Load().append(
#        ('stress', +TENS1*600, 1, 20),
#        ('strain_cumul', .02, 1, 60),
#        ('stress', -TENS1*1000, 1, 20),
#        ('strain_cumul', .02, 1, 100),
#        ('stress', +TENS1*1000, 1, 10),
#        ('strain_cumul', .02, 1, 60),
#        )

# The load instance below is a cross loading
LOAD = so.Load().append(
        ('stress', +TENS2*600, 1, 20),
        ('strain_cumul', .01, 1, 10),
        #('stress', -TENS2*877, 1, 10),
        ('strain', ZERO, 1, 1),
        ('stress', +TENSS*200, 1, 10),
        ('strain_cumul', .03, 1, 30),
        ('strain_cumul', .03, 1, 30),
        )

######################################################################
# 2) SOLVING
######################################################################

SOL = MX.load_with(LOAD)

######################################################################
# 3) PLOTTING
######################################################################

# We plan to make two animated subplots:
# - on top the deviatoric plane action,
# - at the bottom the evolving hardening rule.

# (i) Store quantities of interest in comfy variables ----------------

T = SOL['time']
P = SOL['strain_cumul']
S = np.vectorize(so.Tensor.norm)(SOL['stress'])


# This simplifies the generation of graphs for `somech` objects
PIPLANE = so.Graph('PI-PLANE')

SFUN = MX.equivalent_stress
SFUN.parameters_update(**SOL['added_int_var'][1])

# (ii) Trace the first subplots --------------------------------------

FIG = make_subplots(rows=2, specs=[[{'type': 'polar'}], [{'type': 'xy'}]],)

# On the top figure, traces 0, 1 and 2
# (indices are important for frame updates)
FIG.add_trace(
        PIPLANE.trace(SFUN, line=dict(color='black', width=10), name='heeey',),
        row=1, col=1,
)
FIG.add_trace(
        PIPLANE.trace(SFUN.parameters['h']*SOL['stress'][0].norm()*1.1,
                      line=dict(color='black', width=1),
                      mode="lines+markers+text",
                      text=['0', 'h'], textposition="bottom center",
                      name='h',),
        row=1, col=1,
)
FIG.add_trace(
        PIPLANE.trace(SOL['stress'][0],
                      line=dict(color='red', width=5),
                      mode="lines+markers+text",
                      text=['', str(SOL['stress'][0])],
                      name='stress',),
        row=1, col=1,
)

# At the bottom (row=2), trace 3
FIG.add_trace(
        go.Scatter(x=P, y=S, mode='markers', name='work-hardening',),
        row=2, col=1)


# (iii) Build a FRAME object -----------------------------------------

# We need an iterable of `go.Frame` objects, one for each instant
FRAMES = []
for increment, value in enumerate(SOL['time']):
    # Store the current yield stress
    hr_val = MX.hardening_rule(SOL['strain_cumul'][increment])
    # Update to the new equivalent stress shape
    SFUN.parameters_update(**SOL['added_int_var'][increment])
    FRAMES += [
        go.Frame(
            #name=increment,# can be reused in updatemenu
            # data is a list of traces (/!\ the order matters)
            data=[
                PIPLANE.trace(SFUN, options={'value':hr_val}),
                PIPLANE.trace(SFUN.parameters['h']*hr_val*1.1),
                PIPLANE.trace(
                    SOL['stress'][increment],
                    text=['', str(SOL['stress'][increment])],
                ),
                go.Scatter(x=P[0:increment], y=S[0:increment],),
            ],
            # the following list tells us which of the initial trace we shall replace
            traces=[0, 1, 2, 3],
        )
    ]

# Now we can assign it to the figure instance
FIG.frames = FRAMES

# (iv) Embellish and show ---------------------------------------------

# Button specifications
BUTTON = dict(
    label='Play',
    method='animate',
    args=[
        None,#[f'{k}' for k in range(100)],
        dict(
            frame=dict(duration=80, redraw=True),
            transition=dict(duration=10),
            omcurrent=True,
            mode='immediate'
        ),
    ],
)

# Final touch to the layout
FIG.update_layout(
        updatemenus=[dict(
            type='buttons',
            showactive=False,
            #y=0,
            #x=1.05,
            #xanchor='left',
            #yanchor='bottom',
            buttons=[BUTTON],
        ),],
        title="HAH 2020 (DP780), non-linear loading path",
        xaxis_title="accumulated plastic strain (-)",
        yaxis_title="equivalent stress (MPa)",
        width=1000,
        legend_title="plots",
        font=dict(size=18,),
)

FIG.show()
