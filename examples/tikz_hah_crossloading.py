
"""
This script is an example of definition, computation and visualizatioin using the hah2020 model.
We use the parameters identified for DP780 in Barlat et al. (2020).
"""

import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import somech as so

# Instantiate the material_plastic_distortional object
MX = so.PlasticDistortional(
        # First argument: a hardening rule instance
        so.HardeningRule.hocket_sherby(
            sig_y=425.39,
            sig_s=1257.3,
            eps_eta=1./3.2862,
            eta=.4443
        ),
        # Second argument: an equivalent stress instance
        so.EquivalentStress.hah2020(**{
            # Values from Barlat2020 for DP780
            'k_1': 150, 'k_2': 150, 'k_3': .4, 'k_4': 1, 'k_5': 0,
            'xi_S': 1., 'C': .4, 'k_C': 150.0,#137.6
            'k_C^p': 150.,
            # Recommended values from Barlat2020 (general purpose)
            'q': 3., 'p': 3., 'k': 150., 'k^p': 75., 'xi_R': 1., 'xi_B': 1.,
            'k_S': 150., 'xi_C': 4., 'xi_C^p': 1., 'k_L': 0., 'xi_L': 1, 'xi_L^p': 1
            ## Values from Barlat2020 for DP780
            #'k_1': 58.99, 'k_2': 86.81, 'k_3': .2696, 'k_4': .8072, 'k_5': 9.535,
            #'xi_S': 2., 'C': .4, 'k_C': 250.0,#137.6
            #'k_C^p': 90.10,
            ## Recommended values from Barlat2020 (general purpose)
            #'q': 3., 'p': 3., 'k': 150., 'k^p': 75., 'xi_R': 8., 'xi_B': 1.5,
            #'k_S': 150., 'xi_C': 4., 'xi_C^p': 1., 'k_L': 60., 'xi_L': .5, 'xi_L^p': .25
        }),
)
# HAH2020 is a heavy model that calls (hence stores) the hardening rule
# We can only assign it after the instantiation of MX because it refers to itself
MX.equivalent_stress.parameters_update(
        hardrule=MX.hardening_rule,)

# Loading definition -------------------------------------------------

# Define some tensorial directions to re-use later
ZERO = so.Tensor([0, 0, 0, 0, 0, 0])
TENS2 = so.Tensor([0, 1, 0, 0, 0, 0])
TENSS = so.Tensor([1, 0, -.99, 0, 0, 0]).normed
#TENSS = so.Tensor([1, 0, -1.01, 0, 0, 0]).normed

# We must define the initial state, that is affect a first value to each of the stored variables.
MX.initial_state = dict(
    strain=ZERO,
    strain_cumul=0.,
    stress=ZERO,
    added_int_var=MX.equivalent_stress.parameters,
)

# The load instance below is a cross loading
LOAD = so.Load().append(
        ('stress', +TENS2*600, 1, 1),
        ('strain_cumul', .02, 1, 10),
        ('strain', ZERO, 1, 1),
        ('stress', +TENSS*200, 1, 1),
        ('strain_cumul', .040, 1, 45),
        #('strain_cumul', .015, 1, 23),
        #('strain_cumul', .028, 1, 23),
        )

# SOLVE ==============================================================

SOL = MX.load_with(LOAD)

# PLOT ===============================================================

SOL = SOL[12:]
T = SOL['time']
P = SOL['strain_cumul']
S = np.vectorize(so.Tensor.norm)(SOL['stress'])

SFUN = MX.equivalent_stress
SFUN.parameters_update(**SOL['added_int_var'][1])

FIG = go.Figure()
CANVAS = so.visualization.Graph('deviatoric plane')

for increment, value in enumerate(SOL['time']):
    # Store the current yield stress
    hr_val = MX.hardening_rule(SOL['strain_cumul'][increment])
    # Update to the new equivalent stress shape
    SFUN.parameters_update(**SOL['added_int_var'][increment])

    #CANVAS.tikz_export(SFUN.parameters['h'])
    CANVAS.tikz_export(SFUN, options=dict(amount=100,
                                          value=1.,
                                          style='',)
                      )
