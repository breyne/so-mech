"""
This script is an example of definition, computation and visualization
of a perfect plastic isotropic model.
"""
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import somech as so

# Define tensorial directions
ZERO = so.Tensor([0, 0, 0, 0, 0, 0])
TENS1 = so.Tensor([1, 0, 0, 0, 0, 0])
TENS2 = so.Tensor([0, 1, 0, 0, 0, 0])

# Define material behavior: plastic isotropic
MX = so.PlasticIsotropic(
        so.HardeningRule.hocket_sherby(
            sig_y=425.39,
            sig_s=1257.3,
            eps_eta=1./3.2862,
            eta=.4443
            ),
        so.EquivalentStress.mises(),
        )
MX.initial_state = dict(
    strain=ZERO,
    strain_cumul=0.,
    stress=ZERO
    )
LOAD = so.Load().append(
        ('stress', +TENS1*600, 1, 20),
        ('strain_cumul', .2, 1, 50),
        ('stress', -TENS1*2500, 1, 20),
        ('strain_cumul', .2, 1, 50),
        )

SOL = MX.load_with(LOAD)

T = SOL['time']
P = SOL['strain_cumul']
S = np.vectorize(so.Tensor.norm)(SOL['stress'])
EPS = SOL['strain']**TENS1
SIG = SOL['stress']**TENS1

PIPLANE = so.Graph('piplane')

# Trace normal subplots
FIG = make_subplots(rows=2, specs=[[{'type': 'polar'}], [{'type': 'xy'}]],)
FIG.add_trace(
        PIPLANE.trace(MX.equivalent_stress,
                      line=dict(color='black', width=10), name='heeey',),
        row=1, col=1)
FIG.add_trace(
        PIPLANE.trace(SOL['stress'][0],
                      line=dict(color='red'),
                      mode="lines+markers+text",
                      text=['', str(SOL['stress'][0])],
                      name='stress',),
        row=1, col=1)
FIG.add_trace(
        go.Scatter(x=P, y=S, mode='markers',),
        row=2, col=1)
#FIG.add_trace(go.Scatter(x=EPS, y=SIG, mode='markers',), row=2, col=1)

FIG.update_yaxes(title_text="yaxis 1 title", row=1, col=1)

frames = []
for increment, value in enumerate(SOL['time']):
    #MX.parameters_update(**param)
    hr_val = MX.hardening_rule(SOL['strain_cumul'][increment])
    frames += [
        go.Frame(
            #name=increment,# can be reused in updatemenu
            data=[
                PIPLANE.trace(MX.equivalent_stress, options={'value':hr_val}),
                PIPLANE.trace(SOL['stress'][increment],
                      line=dict(color='red'),
                      mode="lines+markers+text",
                      text=['', str(SOL['stress'][increment])],),
                go.Scatter(x=P[0:increment], y=S[0:increment],),],
            traces=[0,1,2,],)]

FIG.frames = frames
button = dict(
    label='Play',
    method='animate',
    args=[
        None,#[f'{k}' for k in range(100)],
        dict(
            frame=dict(duration=50, redraw=True),
            transition=dict(duration=10),
            omcurrent=True,
            mode='immediate')])

FIG.update_layout(
        updatemenus=[dict(
            type='buttons',
            showactive=False,
            #y=0,
            #x=1.05,
            #xanchor='left',
            #yanchor='bottom',
            buttons=[button])],)

FIG.update_layout(
        PIPLANE.layout,
)

FIG.show()

