#!/bin/python3
"""Generate interactive plots of equivalent Stresses

use from command line:

    ./interactive_yield_functions <mises, tresca, hill48, yld2004, hah2020>

If no argument is provided then the default is haha2020.
Dash is running on http://127.0.0.1:8050/

"""
import sys
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objects as go
import numpy as np
import somech as so




CHOICE = 'hah2020' if len(sys.argv) == 1 else sys.argv[1]

DICT_DB = dict(
        mises={
            'header_div': 'mises',
            'function': so.EquivalentStress.mises(),
            'param_keys': None,
            'params_div': html.Div('No parameter.'),
            },
        tresca={
            'header_div': 'tresca',
            'function': so.EquivalentStress.tresca(),
            'param_keys': None,
            'params_div': html.Div('No parameter.'),
            },
        hill48={
            'header_div': 'hill48',
            'function': so.EquivalentStress.hill48(),
            'param_keys': ('F', 'G', 'H', 'L', 'M', 'N',),
            'params_div': html.Div([
                html.Label('F'), dcc.Input(id='F', value=1., type='number', step=.1),
                html.Label('G'), dcc.Input(id='G', value=1., type='number', step=.1),
                html.Label('H'), dcc.Input(id='H', value=1., type='number', step=.1),
                html.Label('L'), dcc.Input(id='L', value=1., type='number', step=.1),
                html.Label('M'), dcc.Input(id='M', value=1., type='number', step=.1),
                html.Label('N'), dcc.Input(id='N', value=1., type='number', step=.1),
                ], style={'columnCount': 2}),
            },
        yld2004={
            'header_div': 'yld2004',
            'function': so.EquivalentStress.yld2004(),
            'param_keys': (
                'a',
                'c`12', 'c`13', 'c`23', 'c`21', 'c`31', 'c`32', 'c`44', 'c`55', 'c`66',
                'c``12', 'c``13', 'c``23', 'c``21', 'c``31', 'c``32', 'c``44', 'c``55', 'c``66',
                ),
            'params_div': html.Div([
                html.Label('a'), dcc.Input(id='a', value=4., type='number', step=1.),
                html.Label('c`12'), dcc.Input(id='c`12', value=1., type='number', step=.1),
                html.Label('c`13'), dcc.Input(id='c`13', value=1., type='number', step=.1),
                html.Label('c`21'), dcc.Input(id='c`21', value=1., type='number', step=.1),
                html.Label('c`31'), dcc.Input(id='c`31', value=1., type='number', step=.1),
                html.Label('c`32'), dcc.Input(id='c`32', value=1., type='number', step=.1),
                html.Label('c`23'), dcc.Input(id='c`23', value=1., type='number', step=.1),
                html.Label('c`44'), dcc.Input(id='c`44', value=1., type='number', step=.1),
                html.Label('c`55'), dcc.Input(id='c`55', value=1., type='number', step=.1),
                html.Label('c`66'), dcc.Input(id='c`66', value=1., type='number', step=.1),
                html.Label('c``12'), dcc.Input(id='c``12', value=0, type='number', step=.1),
                html.Label('c``13'), dcc.Input(id='c``13', value=0, type='number', step=.1),
                html.Label('c``23'), dcc.Input(id='c``23', value=0, type='number', step=.1),
                html.Label('c``21'), dcc.Input(id='c``21', value=0, type='number', step=.1),
                html.Label('c``31'), dcc.Input(id='c``31', value=0, type='number', step=.1),
                html.Label('c``32'), dcc.Input(id='c``32', value=0, type='number', step=.1),
                html.Label('c``44'), dcc.Input(id='c``44', value=0, type='number', step=.1),
                html.Label('c``55'), dcc.Input(id='c``55', value=0, type='number', step=.1),
                html.Label('c``66'), dcc.Input(id='c``66', value=0, type='number', step=.1),
                ], style={'columnCount': 3}),
            },
        hah2020={
            'header_div': r'$\beta_{1c} = 25 \pm 11 \text{ km s}^{-1}$',
            'function': so.EquivalentStress.hah2020(h=so.Tensor([1, 0, 0, 0, 0, 0])),
            'param_keys': ('h', 'q', 'p', 'g_L', 'g_C', 'f+', 'f-'),
            'params_div': html.Div([
                html.Label('h (angle)'), dcc.Input(id='h', value=30., type='number', step=5.),
                html.Label('q'), dcc.Input(id='q', value=4., type='number', step=1.),
                html.Label('p'), dcc.Input(id='p', value=4., type='number', step=1.),
                html.Label('g_L'), dcc.Input(id='g_L', value=1., type='number', step=.1),
                html.Label('g_C'), dcc.Input(id='g_C', value=1., type='number', step=.1),
                html.Label('f+'), dcc.Input(id='f+', value=0., type='number', step=.1),
                html.Label('f-'), dcc.Input(id='f-', value=0., type='number', step=.1),
                ], style={'columnCount': 3}),
            'special_update': dict(
                h=lambda val: so.Tensor.angle2deviator(val/180*np.pi),
                ),
            },
        francois2001={
            'header_div': r'FRANCOIS 2001',
            'function': so.EquivalentStress.francois2001(X=so.Tensor([1, 0, 0, 0, 0, 0])),
            'param_keys': ('X', 'K0', 'KX', 'p0', 'pX'),
            'params_div': html.Div([
                html.Label('backstress'),
                html.Label('tensor'),
                html.Label('X (angle)'), dcc.Input(id='X', value=0., type='number', step=5.),
                html.Label('K0'), dcc.Input(id='K0', value=0., type='number', step=.1),
                html.Label('KX'), dcc.Input(id='KX', value=0., type='number', step=.1),
                html.Label('p0'), dcc.Input(id='p0', value=2., type='number', step=.1),
                html.Label('pX'), dcc.Input(id='pX', value=2., type='number', step=.1),
                ], style={'columnCount': 3}),
            'special_update': dict(
                X=lambda val: so.Tensor.angle2deviator(val/180*np.pi),
                ),
            },
        )
MODEL = DICT_DB[CHOICE]
MODEL.setdefault('special_update', dict())

CANVAS = so.visualization.Graph('deviat')
FUN = DICT_DB[CHOICE]['function']
#FUN.polar_curve = FUN.polar_curve_homogeneous
KEYS = MODEL['param_keys']
HEADER = MODEL['header_div']
PARAMS = MODEL['params_div']

EXTSS = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
APP = dash.Dash(__name__, external_stylesheets=EXTSS)

APP.layout = html.Div([
    HEADER,
    dcc.Graph(figure=go.Figure(CANVAS.trace(FUN)), id='mygraph'),
    PARAMS,
], style={'columnCount': 1})

if KEYS is not None:
    @APP.callback(
        Output(component_id='mygraph', component_property='figure'),
        [Input(component_id=key, component_property='value') for key in KEYS]
    )
    def update_from_param_change(*args):
        """DOCT"""
        for num, val in enumerate(args):
            key = KEYS[num]
            if key in MODEL['special_update'].keys():
                func = MODEL['special_update'][key]
                FUN.parameters_update(**{key:func(val)})
            else:
                FUN.parameters_update(**{key:val})

        fig = go.Figure(CANVAS.trace(FUN))
        fig.add_trace(CANVAS.trace(so.Tensor([1, 0, 0, 0, 0, 0]).dev))
        fig.add_trace(CANVAS.trace(so.Tensor([0, 1, 0, 0, 0, 0]).dev))
        fig.add_trace(CANVAS.trace(so.Tensor([0, 0, 1, 0, 0, 0]).dev))
        return fig

if __name__ == '__main__':
    APP.run_server(debug=True)#, suppress_callback_exceptions=True)
