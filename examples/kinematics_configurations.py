import numpy as np
import plotly.graph_objects as go
import somech as so
import pandas as pd

#DEFO = so.kinematics.PURE_SHEAR
#DEFO = so.kinematics.ROTATION
#DEFO = so.kinematics.TENSION_ISOCHORIC
DEFO = so.kinematics.SIMPLE_SHEAR
#DEFO.d_t/=2

CANVAS = so.Graph('xy')

FRAMES = []
T_TOT = 2.

#one = so.tensors.one_d3o2
MODELS = pd.DataFrame({
    'name':['current', 'Green', 'Jaumann', 'logarithmic'],
    'color':['gray', 'red', 'green', 'blue' ],
    })

for res in DEFO.generate(T_TOT, DO=MODELS['name']):
    #FRAMES += []
    FRAMES += [go.Frame(data=[
        CANVAS.trace(
            res["deformation_gradient"][mod['name'].lower()],
            {'color':mod['color'], 'name':mod['name']})
        for row, mod in MODELS.iterrows()
        ])]

# Assemble figure

T_ANIM = 2.
N_INC = len(FRAMES)
DT=T_TOT/N_INC
FPS=N_INC/T_ANIM

if len(FRAMES)>T_ANIM*30:
    FRAMES = FRAMES[1::int(np.ceil(len(FRAMES)/(T_ANIM*30)))]

FIG = go.Figure(
    data=FRAMES[0]['data'],
    layout=go.Layout(
        xaxis={'range':[-1, 3], 'autorange':False, 'zeroline':False, 'visible':False},
        yaxis={'range':[-1, 3], 'autorange':False, 'zeroline':False, 'visible':False},
        title_text="TIME RANGE 0-"+str(T_TOT),
        font=dict(
            family="Fira sans",
            size=18,
            color="darkslategrey",
        ),
        #hovermode="closest",
        updatemenus=[dict(
            type="buttons",
            buttons=[dict(
                label="Play",
                method="animate",
                args=[None, {
                    "frame":{"duration": 1000/FPS, "redraw": False},
                    "fromcurrent": True,
                    "transition": {"duration": 0}
                }]
            ),
                     {
            "args": [[None],
                     {"frame": {"duration": 0, "redraw": False},
                      "mode": "immediate",
                      "transition": {"duration": 0}}
                     ],
                "label": "Pause",
                "method": "animate"
                }]
        )],
        #showlegend=False,
        height=1000,
        width=1000,
    ),
    frames=FRAMES,
)

FIG.show()
