"""
This script illustrate how somech.Tensor instances can be manipulated,
using the example of 2D simple-shear.
- We compute a few tensors and optimize their storage.
- We compute a few decompositions.
- We illustrate the motion on an animated figure.
"""

import numpy as np
import somech as so
import plotly.graph_objects as go

GAMMA_MAX = 1
DURATION = 3# seconds
FPS = 40# frame per second
N = DURATION*FPS

#pylint: disable=invalid-name
#pylint: disable=too-many-locals
def data_fig_sheared_square(gamma):
    """Return the contents of Plotly frame

    Gamma is the shear parameter defining the deformation gradient
        F = | 1     0 |
            | gamma 1 |
    We return a list of Plotly traces composed of:
    - the initial configuration (unit square);
    - the current conifiguration (sheared square);
    - the eigendirections of stretches.
    """
    # The simple shear deformation gradient: by definition,
    F = so.Tensor([[1, 0], [gamma, 1]])

    #The right (C) and left (b) Cauchy-Green tensors
    C = F.T * F
    b = F * F.T
    # Since both C and b are symmetric by essence,
    # let them be stored in a symmetric tensorial basis.
    C.tensorize_basis()
    b.tensorize_basis()

    # Now we want the Lagrangian (U) and Eulerian (V) stretch tensors.
    # Let us follow two different approaches.

    # For U, we'll use the square roots of the eigenvalues of C
    # First compute the eigen-decomposition
    C_vals, C_vecs = C.decomposition_eigen()
    # Then recreate the tensor with same direction and root-ed values
    U = C_vals[0]**.5 * (C_vecs[0]%C_vecs[0]).in_tens_basis() +\
        C_vals[1]**.5 * (C_vecs[1]%C_vecs[1]).in_tens_basis()

    # For V, we use directly the polar decomposition
    _R, V = F.decomposition_polar(side='left')

    _u_vals, u_vecs = U.decomposition_eigen()
    u11 = u_vecs[0].values[0]#*C_vals[0]**.5
    u12 = u_vecs[0].values[1]#*C_vals[0]**.5
    u21 = u_vecs[1].values[0]#*C_vals[1]**.5
    u22 = u_vecs[1].values[1]#*C_vals[1]**.5

    v_vals, v_vecs = V.decomposition_eigen()
    v11 = v_vecs[0].values[0]*v_vals[0]
    v12 = v_vecs[0].values[1]*v_vals[0]
    v21 = v_vecs[1].values[0]*v_vals[1]
    v22 = v_vecs[1].values[1]*v_vals[1]

    rbm = so.Tensor([0, 1])*F.values[1, 0]
    rbmy = rbm.values[1]

    x_a = F*XA + rbm
    x_b = F*XB + rbm
    x_c = F*XC + rbm
    x_d = F*XD + rbm

    return [
        go.Scatter(
            x=[XA.values[0], XB.values[0], XC.values[0], XD.values[0]],
            y=[XA.values[1], XB.values[1], XC.values[1], XD.values[1]],
            fill="toself", line_width=0,#fillcolor='gray'
        ),
        go.Scatter(x=[-u11, u11], y=[-u12, u12], line_color='blue', line_width=1,),
        go.Scatter(x=[-u21, u21], y=[-u22, u22], line_color='blue', line_width=1,),
        go.Scatter(
            x=[x_a.values[0], x_b.values[0], x_c.values[0], x_d.values[0]],
            y=[x_a.values[1], x_b.values[1], x_c.values[1], x_d.values[1]],
            fill="toself", line_color='gray', line_width=0,
        ),
        go.Scatter(x=[-v11, v11], y=[rbmy-v12, rbmy+v12], line_color='black'),
        go.Scatter(x=[-v21, v21], y=[rbmy-v22, rbmy+v22], line_color='black'),
        go.Scatter(
            x=[x_c.values[0]],
            y=[x_c.values[1]],
            mode="text",
            text=" Gamma = "+'{0:.2f}'.format(gamma),
            textposition='bottom right',
            textfont_size=20,
        ),
    ]
FRAMES = []

XA = so.Tensor([-1, -1])
XB = so.Tensor([-1, +1])
XC = so.Tensor([+1, +1])
XD = so.Tensor([+1, -1])

for n, gam in enumerate(np.arange(N)/(N-1)*GAMMA_MAX):
    FRAMES += [go.Frame(data=data_fig_sheared_square(gam))]

# Now let us illustrate that on a neat animated plot.

FIG = go.Figure(
    data=data_fig_sheared_square(.5)+[
        go.Scatter(
            x=[1.25], y=[.75],
            mode="text",
            text="Note that\
            <br>the principal stretches change orientation\
            <br>in the reference configuration.",#pylint: disable=line-too-long
            textposition='bottom right'
        ),
    ],
    layout=go.Layout(
        xaxis=dict(range=[-1, 3], autorange=False, zeroline=False, visible=False),
        yaxis=dict(range=[-1, 3], autorange=False, zeroline=False, visible=False),
        title_text=" Simple shear deformation",
        font=dict(
            family="Fira sans",
            size=18,
            color="darkslategrey",
        ),
        #hovermode="closest",
        updatemenus=[dict(
            type="buttons",
            buttons=[dict(
                label="Play",
                method="animate",
                args=[None, {
                    "frame":{"duration": 1000/FPS, "redraw": False},
                    "fromcurrent": True,
                    "transition": {"duration": 0}
                }]
            )]
        )],
        showlegend=False,
        height=1000,
        width=1000,
    ),
    frames=FRAMES
)


FIG.show()
