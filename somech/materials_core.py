"""General definitions for material constitutive models

Here is a bunch of utility classes to set basic concepts for materials.
This module is useless to the user but not to the contributor.

Notes
-----
- we try to apply the "composition over inheritance" principle,
- yet inheritance is important for implementation requirement by abstraction.
- What really governs the hierarchy is the *solver type* more than the physics.
  For example what unifies `Plastic` models is the return mapping algorithm.
"""

import os
import sys
import abc
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from somech import tensors #pylint: disable=wrong-import-position
Tensor = tensors.Tensor


class ConstitutiveModel(abc.ABC):#pylint: disable=too-few-public-methods
    """Constitutive Model abstract base class

    This is a class from which derive all constitutive model classes like Elastic, plastic, etc.


    Attributes
    ----------
    dim : int
        The problem's dimension: 1 or 2 or 3.
    var_types : np.dtype
        Variables of interest by name and type.
    initial_state : structured ndarray
        The array's dtype is: var_types.
        assign with dict
        An initial value for each of the `var_types`.

    Notes
    -----
    Derived classes should imperatively:
        - define variables of interest as dtypes
        - define a solver
    """
    def __new__(cls, *args, **kwargs):#pylint: disable=unused-argument
        """Construct instances with default attributes.

        Attach a default `dim`ension and empty `initial_state` to all instances
        The arguments are unused.
        """
        instance = super(ConstitutiveModel, cls).__new__(cls)
        instance._dim = 3
        instance._initial_state = None
        return instance

    def __repr__(self):
        """Display class name and list attributes

        Example
        -------
        >>> ExampleModel()# This is a dummy class for the example
        ExampleModel instance with attributes:
    	- _dim = 3
    	- _initial_state = None
        """
        rep = self.__class__.__name__+' instance with attributes:'
        for key, value in self.__dict__.items():
            rep += '\n- '+key+' = '+str(value)
        return rep

    @property
    @abc.abstractmethod
    def var_types(self):
        """np.dtype : variables of the model by name and type

        A definition looks like

            np.dtype([
                ('var1_name', var1_Type),
                ...                     ,
                ('varN_name', varN_Type)
                ])

        Example
        -------
        >>> EM = ExampleModel()# This is a dummy class for the example
        >>> print(EM.var_types)
        [('temperature', '<f8'), ('stress', 'O')]

        In the above example, the field 'temperature' will store floats
        and 'stress' will store objects.
        """

    @abc.abstractmethod
    def solve(self, sols_old, sols_new):
        """
        Arguments
        ---------
        sols_old : structured ndarray of len 1
            Solutions at last known increment.
        sols_new : structured ndarray of len 1
            Solutions at next increments.
            Only imposed values are set.
        """

    @property
    def dim(self):
        """The problem's dimension: 1 or 2 or 3"""
        return self._dim

    @dim.setter
    def dim(self, value):
        """Check before assigning"""
        if value in (1, 2, 3):
            self._dim = value
        else:
            raise ValueError('Attribute `dim` should be in (1, 2, 3).')

    @property
    def initial_state(self):
        """An initial value for each of the `var_types`

        It is assigned with a dict the keys correspond to the `var_types`.
        A time field with value 0. is automatically prepended to the initial state.

        Example
        -------
        >>> EM = ExampleModel()# This is a dummy class for the example
        >>> print(EM.var_types)
        [('temperature', '<f8'), ('stress', 'O')]
        >>> EM.initial_state = dict(temperature=373., stress=Tensor([0, 0, 0],2))
        >>> print(EM.initial_state)
        [(0., 373., [0. 0. 0.]_(2D,O2))]
        >>> print(EM.initial_state.dtype)
        [('time', '<f8'), ('temperature', '<f8'), ('stress', 'O')]
        """
        return self._initial_state

    @initial_state.setter
    def initial_state(self, vals_dict):
        """Assign initial state with a dictionnary."""
        # Construct as an empty structured array, full of nans
        # We append time to the fields of interest
        self._initial_state = self._empty_states(1)
        # Fill with values
        self._initial_state['time'] = 0.
        for key, val in vals_dict.items():
            self._initial_state[key] = val
        # Make sure nothing is empty
        if False in self._is_set_state(self._initial_state[0]):
            raise TypeError('Nothing should be empty after defining the initial state.')

    def _empty_states(self, size):
        """Return unset states of appropriate types and given size

        Notes
        -----
        The current method could be unsafe as I'm not sure of the behavior of
        np.full with dtype Object.
        A solution would be to require a default `cls.unset_state` to copy everywhere
        and test with.

        See
        ---
        https://stackoverflow.com/a/55848922
        """
        return np.full(size, None, [('time', '<f8')]+self.var_types.descr)

    @staticmethod
    def _is_set_state(state):
        """Return a tuple of booleans saying where the state is set"""
        ret_tup = ()
        # Loop on the state values
        for col, val in enumerate(state):
            # If it's an object we compare with None
            if state.dtype[col] == 'O':
                ret_tup += (False,) if val is None else (True,)
            # Else we test for nan
            else:
                ret_tup += (False,) if np.isnan(val) else (True,)
        return ret_tup

    @staticmethod
    def _which_set_state(state):
        """Return a tuple of names of set variables"""
        idx_set = np.where(ConstitutiveModel._is_set_state(state))
        return np.array(state.dtype.names)[idx_set]


    def load_with(self, load: object):
        """Split the loading into increments and call the solver for each of them.
        Then concatenate and return solutions.
        """
        # Initialize solutions as np structured array of nans
        # We get in trouble in self.initial_state is not defined
        solutions = np.full(
            1 + load.total_nb_of_inc(), # total size
            None,                       # full of nans
            self.initial_state.dtype    # types
            )
        solutions[0] = self.initial_state

        # Loop on increments
        for inc, name, d_t, d_load in load.generate_increments():
            # Assign imposed values to next increment
            solutions[inc+1]['time'] = solutions[inc]['time'] + d_t
            solutions[inc+1][name] = solutions[inc][name] + d_load
            # Send and get solutions
            solutions[inc+1] = self.solve(solutions[inc], solutions[inc+1])

        return solutions

class ExampleModel(ConstitutiveModel):
    """Dummy class for example purpose"""
    var_types = np.dtype([
        ('temperature', float),
        ('stress', Tensor)
        ])
    def __init__(self, *args, **kwargs):
        """Save arguments as attributes"""
        if args:
            self.list = args
        if kwargs:
            self.dict = kwargs
    def solve(self, sols_old, sols_new):
        #import pdb; pdb.set_trace()
        sols_new = sols_old
        return sols_new

class Load():
    """What is passed to a solver (ConstitutiveModel.load_with()) to get a solution

    The user defines a load by appending *steps*, that is periods of constant loading rate.
    Each step is defined by
    - an imposed variable name,
    - an imposed rate,
    - a total duration,
    - a number of increments.
    From this the condition of each increment can be recreated and passed to the solver.
    """
    # Attributes
    # ----------
    # imposed_name : list of strings
    #     E.g.  'stress' or 'strain' or 'temperature', smth the solver can deal with.
    # imposed_rate : list of objects
    #     Typically a float or a Tensor (strain or stress path).
    # duration : list of floats
    #     It can be an effective time or a kinematic one.
    # nb_increments : list of strings
    #     This number divides the duration to get the incremental time.
    def __init__(self):
        """Initialize an empty Load instance"""
        # We use lists because mutability could be useful
        self.imposed_name = []
        self.imposed_rate = []
        self.duration = []
        self.nb_increments = []

    def __repr__(self):
        """Easy printing"""
        rep = self.__class__.__name__+' instance composed of:'
        for step in range(self.count_steps()):
            rep += '\nstep '+str(step)+' : '
            rep += self.imposed_name[step]+' rate '+str(self.imposed_rate[step])
            rep += ' for duration '+str(self.duration[step])
            rep += ' divided into '+str(self.nb_increments[step])+' increments.'
            #rep += '\n'
        return rep

    def append(self, *args):
        """Append a new loading step

        A loading step consists of several increments going in the same direction.
        It several loading steps can be appended at once

        my_load.append(
            (name_1, rate_1, step_duration_1, nb_increments_1),
            (...   , ...   , ...            , ...            ),
            (name_N, rate_N, step_duration_N, nb_increments_N),
            )

        Arguments
        ---------
        args : tuple of iterables
            args[ ][0] : string
                Name of the imposed variable
            args[ ][1] : depends
                Rate of the imposed variable
            args[ ][2] : float
                Total duration of the step
            args[ ][3] : int
                Number of increment: subdivisions of the step

        Example
        -------
        >>> a = ('stress', Tensor([1,0,0],2), 10., 35)
        >>> b = ('strain', Tensor([1,0,0],2), 10., 15)
        >>> c = ('temperature', 1., 10., 5)
        >>> Load().append(a, b, a, c)
        Load instance composed of:
        step 0 : stress rate [1. 0. 0.]_(2D,O2) for duration 10.0 divided into 35 increments.
        step 1 : strain rate [1. 0. 0.]_(2D,O2) for duration 10.0 divided into 15 increments.
        step 2 : stress rate [1. 0. 0.]_(2D,O2) for duration 10.0 divided into 35 increments.
        step 3 : temperature rate 1.0 for duration 10.0 divided into 5 increments.
        """
        for n_step, step in enumerate(args):
            if len(step) != 4:
                raise ValueError(
                    'Arg '+str(n_step)
                    +': expecting an iterable of length 4.'
                    )
            if not isinstance(step[0], str):
                raise ValueError(
                    'Arg '+str(n_step)
                    +': name is expected to be a string.'
                    )
            if step[2] <= 0.:
                raise ValueError(
                    'Arg '+str(n_step)
                    +': duration value is expected to be a positive number.'
                    )
            if not isinstance(step[3], int) or step[3] < 1:
                raise ValueError(
                    'Arg '+str(n_step)
                    +': Number of increments is expected to be a positive integer.'
                    )
            self.imposed_name += [step[0]]
            self.imposed_rate += [step[1]]
            self.duration += [step[2]]
            self.nb_increments += [step[3]]

        # We return self such that one can write load = Load().append(...).append(...) ...
        return self

    def count_steps(self):
        """Count how many loading step have been input"""
        # Also ensure consistency of lengths
        ref_len = len(self.imposed_name)
        if ref_len == len(self.imposed_rate) == len(self.duration) == len(self.nb_increments):
            return ref_len
        raise ValueError("Load instance attributes should all have the same length")

    def total_nb_of_inc(self):
        """Return the sum of the increments of all the steps"""
        return np.array(self.nb_increments).sum()

    def generate_increments(self):
        """Generate (yield) increments of loads"""
        ret_inc = -1
        # Loop on each step
        for step, step_duration in enumerate(self.duration):
            ret_var_name = self.imposed_name[step]
            nb_inc = self.nb_increments[step]
            ret_d_t = 1.*step_duration / self.nb_increments[step]
            ret_d_load = ret_d_t * self.imposed_rate[step]
            for _ in range(nb_inc):
                ret_inc += 1
                yield ret_inc, ret_var_name, ret_d_t, ret_d_load


class ParametricFunction():
    """A function object with parameters and convenient methods

    Attributes
    ----------
    function : callable
        Can be anything.
    parameters : dict
        Some models require parameters p1, ..., pn:
        parameters = {"p1":value1, ..., "p1":valueN}
    """

    def __init__(self, fun_, **params):
        """Initialize parametric function

        Arguments
        ---------
        fun_ : callable(*var, **param)
            This is the only acceptable signature:
            the function is multivariate with posible parameters.
        params : dict
            Optional default parameters
        """
        #if fun_ is None:
        if not callable(fun_):
            raise TypeError(' Class ParametricFunction: expecting a callable for instantiation.')
        self.function = fun_
        self.parameters_reset()
        self.parameters_update(**params)

    def parameters_reset(self):
        """Reset the parameters dictionary to an empty set"""
        self.parameters = {}

    def parameters_update(self, **p):
        """Update parameters: replace and/or add some."""
        self.parameters.update(p)

    def __call__(self, *args):
        if callable(self.function):
            return self.function(*args, **self.parameters)
        raise TypeError('Class ParametricFunction: attribute function should be a callable.')

    def __repr__(self):
        """UTF8 output

        Example
        -------
        >>> ParametricFunction(lambda x, **p: 2*x + p['a'], a=1)
        ParametricFunction
    	- 1 parameters ('a',)
        >>> ParametricFunction(lambda x, **p: x+1)
        ParametricFunction
    	- 0 parameters
        """
        return str(
            self.__class__.__name__
            +'\n- '+str(len(self.parameters)) +' parameters'
            +(' '+str(tuple(self.parameters.keys())) if bool(self.parameters) else '')
            )

if __name__ == "__main__":
    import doctest
    doctest.testmod()
