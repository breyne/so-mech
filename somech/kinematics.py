"""This module sets a framework to describe motion with tensors.

Essentially, it provides ways to characterize a deformation,
then maps it to different configurations. 
"""
import numpy as np
import somech as so
#import plotly.graph_objects as go

# Define configuration changes as decorators for EQS


class LocalDeformation():
    """Holds the prescribed kinematics of a particle, 
    defined by a deformation gradient or a velocity gradient.
    """
    def __init__(self, **kwargs):
        kwargs.setdefault('velocity_gradient', None)
        kwargs.setdefault('deformation_gradient', None)
        kwargs.setdefault('d_t', 5e-3)
        self.dy_dx = None
        self.dv_dx = None
        self.d_t = float(kwargs['d_t'])
        def_is_foo=False
        vel_is_foo=False
        if callable(kwargs['deformation_gradient']):
            self.dy_dx=kwargs['deformation_gradient']
            def_is_foo=True
        if callable(kwargs['velocity_gradient']):
            self.dv_dx=kwargs['velocity_gradient']
            vel_is_foo=True
        if vel_is_foo == def_is_foo == True:
            raise ValueError("""Don't give both F and l
                             (could be incompatible for what I know),
                             prefer F alone.""")
        if vel_is_foo == def_is_foo == False:
            raise ValueError("Must specify "+
                             "`velocity_gradient` or `deformation_gradient` "
                             "in LocalDeformation constructor.")

#    def __repr__(self):
#        """What to print on call"""
#        return "Deformation "

    def both_gradients_generator(self, time):
        """both F and L given time"""
        if callable(self.dv_dx):
            one = so.Tensor.projector('identity',3, order=2)
            def_g = so.Tensor.projector('identity',3, order=2)
            for tau in np.arange(0., time+self.d_t, self.d_t):
                yield def_g, vel_g
                vel_g = self.dv_dx(tau)
                def_g = (one - vel_g*self.d_t).inverse()*def_g
        else: #if callable(self.l):
            for tau in np.arange(0., time+self.d_t, self.d_t):
                def_g = self.dy_dx(tau)
                def_g_dot = (self.dy_dx(tau+self.d_t)-def_g)/self.d_t
                vel_g = def_g_dot*(~def_g)
                yield def_g, vel_g
    def both_gradients(self, time, generator=False):
        """l at given time"""
        return yield_or_return(
                self.both_gradients_generator(time),
                generator)

    def velocity_gradient_generator(self, time):
        """l at given time"""
        if callable(self.dv_dx):
            for tau in np.arange(0., time+self.d_t, self.d_t):
                yield self.dv_dx(tau)
        else: #if callable(self.l):
            for tau in np.arange(0., time+self.d_t, self.d_t):
                def_g = self.dy_dx(tau)
                def_g_dot = (self.dy_dx(tau+self.d_t)-def_g)/self.d_t
                vel_g = def_g_dot*(~def_g)
                yield vel_g

    def velocity_gradient(self, time, generator=False):
        """l at given time"""
        return yield_or_return(
                self.velocity_gradient_generator(time),
                generator)

    def deformation_gradient_generator(self, time):
        """F at given time"""
        #transfo = transfo_old*(one - vgradXdt).inverse()
        if callable(self.dy_dx):
            for tau in np.arange(0., time+self.d_t, self.d_t):
                yield self.dy_dx(tau)
        else: #if callable(self.l):
            #vel_g = self.dv_dx(time)
            one = so.Tensor.projector('identity',3, order=2)
            def_g = so.Tensor.projector('identity',3, order=2)
            for tau in np.arange(0., time+self.d_t, self.d_t):
                yield def_g
                def_g = (one - self.dv_dx(tau)*self.d_t).inverse()*def_g

    def deformation_gradient(self, time, generator=False):
        """F at given time"""
        return yield_or_return(
                self.deformation_gradient_generator(time),
                generator)

    def generate(self, time, **kwargs): # pylint: disable=too-many-locals
        """ Gather all generable in one function"""
        kwargs.setdefault('do', None)
        #kwargs.setdefault('rate_only', False)
        models = {'current', 'jaumann', 'green', 'logarithmic'}
        #tensors = {'def_grad', 'vel_grad'}
        todo = {'current', }
        for key, val in kwargs.items():
            if key.lower() == "do":
                if val is None:
                    continue
                for model in val:
                    assert model.lower() in models
                    todo.add(model.lower())
            #elif key == "rate_only":
            #    pass
            else:
                raise KeyError(f"Wrong key {key} passed to function")
        # --------------------------------------------------------------
        one = so.tensors.one_d3o2
        res = { "deformation_gradient":{}, "velocity_gradient":{}}
        conf_g = one*1.
        conf_j = one*1.
        conf_l = one*1.
        lnv_old = one*0.
        conf_g_old = one*1.
        for dgrad, vgrad in self.both_gradients_generator(time):
            polar_done = False
            if "current" in todo:
                res["deformation_gradient"]["current"] = dgrad
                res["velocity_gradient"]["current"] = vgrad
            if "green" in todo:
                res["deformation_gradient"]["green"] = conf_g*1.
                conf_g, polar_v = dgrad.decomposition_polar("left")
                polar_done = True
                r_dot = (conf_g-conf_g_old)/self.d_t
                res["velocity_gradient"]["green"] = r_dot*conf_g.inverse()
                conf_g_old = conf_g*1
            if  "jaumann" in todo:
                res["deformation_gradient"]["jaumann"] = conf_j*1
                res["velocity_gradient"]["jaumann"] = vgrad.skew
                conf_j = vel_grad_integrate(vgrad.skew, self.d_t, conf_j)
            if "logarithmic" in todo:
                res["deformation_gradient"]["logarithmic"] =conf_l
                if not polar_done:
                    _, polar_v = dgrad.decomposition_polar("left")
                lnv = polar_v.log
                d_lnv = (lnv-lnv_old)/self.d_t
                w_log, _ = spin_numint(lnv, d_lnv, vgrad.sym)
                res["velocity_gradient"]["logarithmic"] = w_log
                conf_l = vel_grad_integrate(w_log, self.d_t, conf_l)
                lnv_old = lnv*1
            yield res

    def configuration_green_generator(self, time):
        """Green config R at given time"""
        #tau=0.
        for def_g in self.deformation_gradient(time, generator=True):
            rot, _ = def_g.decomposition_polar('right')
            yield rot

    def configuration_green(self, time, generator=False):
        """Green config R at given time"""
        return yield_or_return(
                self.configuration_green_generator(time),
                generator)

    def configuration_jaumann_generator(self, time):
        """Jaumann config R* at any time"""
        #transfo = transfo_old*(one - vgradXdt).inverse()
        one = so.Tensor.projector('identity',3, order=2)
        rot = so.Tensor.projector('identity',3, order=2)
        for vel_g in self.velocity_gradient(time, generator=True):
            yield rot
            rot = (one - vel_g.skew*self.d_t).inverse()*rot

    def configuration_jaumann(self, time, generator=False):
        """Jaumann config R at given time"""
        return yield_or_return(
                self.configuration_jaumann_generator(time),
                generator)

    def configuration_logarithmic_generator(self, time):
        # pylint: disable=invalid-name
        """Implemented after Xiao, Bruhns and Meyers (Acta Mechaica, 1997)"""
        one = so.Tensor.projector('identity',3, order=2)
        rot = so.Tensor.projector('identity',3, order=2)
        lnV_old = one*0.
        for def_g in self.deformation_gradient(time, generator=True):
            yield rot
            vel_g = self.velocity_gradient(time)
            W_LOG = spin_log(def_g, vel_g)
            # -------------------
            _, V = def_g.decomposition_polar(side='left')
            lnV = V.log
            d_lnV = (lnV-lnV_old)/self.d_t
            W_LOG, error = spin_numint(lnV, d_lnV, vel_g.sym)
            lnV_old = lnV*1.
            if error>1e-2:
                print(Warning(f'Error = {error} !!!'))
            # -------------------
            #print(N_LOG)
            #import pdb; pdb.set_trace()
            rot = (one - (W_LOG)*self.d_t).inverse()*rot

    def configuration_logarithmic(self, time, generator=False):
        """LOG"""
        return yield_or_return(
                self.configuration_logarithmic_generator(time),
                generator)



def yield_or_return(the_gen:callable, generator=False):
    """Return the generator or its last item"""
    if generator:
        return the_gen
    *_, res = the_gen
    return res

def yield_or_return_old(the_gen:callable, generator=False):
    """Return the generator or its last item
    The try catch can handle empty lists.
    Kind of a user's problem here.
    """
    if generator:
        return the_gen
    try:
        *_, res = the_gen
    except ValueError:
        res = list(the_gen)
    return res

ROTATION = LocalDeformation(
        velocity_gradient=lambda t: so.Tensor(
            [[0.,1,0],
            [-1.,0.,0],
            [ 0.,0.,0],
            ], 3)
        )
SIMPLE_SHEAR = LocalDeformation(
        deformation_gradient=lambda t: so.Tensor(
            [[1. ,t,0],
            [0. ,1.,0],
            [0. ,0.,1],
            ], 3)
        )
TENSION_ISOCHORIC = LocalDeformation(
        deformation_gradient=lambda t: so.Tensor(
            [[2.,0 ,0],
             [0 ,1.,0],
             [0.,0.,1],
            ], 3) * t/2
        )
PURE_SHEAR = LocalDeformation(
        deformation_gradient=lambda t: so.Tensor(
            [[1.,t ,0],
             [t ,1.,0],
             [0.,0.,1],
            ], 3) if t<1. else so.Tensor(
            [[1.,.999999 ,0],
             [.999999 ,1.,0],
             [0.,0.,1],
            ], 3)
        )

def spin_log(def_g, vel_g):
    r"""Logarthmic spin
    after Xiao, Bruhns and Meyers (Acta Mechanica, 1997)
    
    Parameters
    ----------
    def_g : Tensor, 2D, order 3
            The deformation gradient.
    vel_g : Tensor, 2D, order 3
            The velocity gradient.

    Returns
    -------
    W_LOG : The logarithmic spin
            Such that \dot{ln V} + (ln V).W_LOG - W_LOG.(ln V) = D
    """
    # pylint: disable=invalid-name
    one = so.Tensor.projector('identity',3, order=2)
    _, V = def_g.decomposition_polar(side='left')
    V_vals, _ = V.decomposition_eigen()
    D, W = vel_g.sym, vel_g.skew
    B = V*V.T
    # Cauchy--green left i V**2
    B_vals = np.array(V_vals)**2.
    N_LOG = one*0.
    if B_vals[0] == B_vals[1] == B_vals[2]:
        N_LOG *= 0.
    elif B_vals[0] != B_vals[1] == B_vals[2]:
        nu = 1/(B_vals[0]-B_vals[1])*(
                (1+B_vals[0]/B_vals[1])/(1-B_vals[0]/B_vals[1])
                +2/np.log(B_vals[0]/B_vals[1])
                )
        N_LOG = nu*B*D
    else:
        Delta = (B_vals[0]-B_vals[1])*(B_vals[1]-B_vals[2])*(B_vals[2]-B_vals[0])
        eps = np.array([B_vals[1]/B_vals[2], B_vals[2]/B_vals[0], B_vals[0]/B_vals[1],])
        nu = np.zeros(3)
        for idx in range(3):
            factor = (1+eps[idx])/(1-eps[idx])+2/np.log(eps[idx])
            nu[0] += (-B_vals[idx])**(2)*factor
            nu[1] += (-B_vals[idx])**(1)*factor
            nu[2] += (-B_vals[idx])**(0)*factor
        nu /= Delta
        N_LOG += nu[0]*(B*D - D*B)
        N_LOG += nu[1]*(B*B*D - D*B*B)
        N_LOG += nu[2]*(B*B*D*B - B*D*B*B)
    return W + N_LOG

def spin_numint(strain, d_strain, sym_vel_g):
    r"""Compute spin W such that 
            \dot{E} + E.W - W.E = D
    
    Parameters
    ----------
    strain  : so.Tensor
        The strain measure E above
    d_strain : so.Tensor
        The spatial derivative \dot{E} above
    sym_vel_g : so.Tensor
        The strain rate tensor, symmetric, D above.

    Returns
    -------
    spin : So.Tensor
        The spin that minimizes the residual of the equation above.
        We have 3 unknowns wi for 6 equations (sym tensor) so thius 
        is the best we can do
    rel_error : float
        The residual size compared to the size of D

    Notes
    -----
    We use the form 
            /  0 -w3  w2 \
        W = | w3   0 -w1 | = wi Bi
            \-w2  w1   0 /
    """
    if strain.norm() == 0.:
        spin = so.Tensor(np.zeros([3,3]), 3)
        error = (sym_vel_g-d_strain).norm()/d_strain.norm()
        return spin, error
    # Base tensors
    b_0 = so.Tensor([[ 0, 0, 0],
                    [ 0, 0,-1],
                    [ 0, 1, 0],], 3)
    b_1 = so.Tensor([[ 0, 0, 1],
                    [ 0, 0, 0],
                    [-1, 0, 0],], 3)
    b_2 = so.Tensor([[ 0,-1, 0],
                    [ 1, 0, 0],
                    [ 0, 0, 0],], 3)
    # Linop
    linop = np.zeros([6, 3])
    linop[:,0] = (strain*b_0-b_0*strain).in_tens_basis().values
    linop[:,1] = (strain*b_1-b_1*strain).in_tens_basis().values
    linop[:,2] = (strain*b_2-b_2*strain).in_tens_basis().values
    # The system is Lw = (e_dot-d), overconstrained so we solve
    # the minimization problem with the pseudoinverse
    w_vec = np.linalg.solve(linop.T.dot(linop),
                            linop.T.dot(
                                (sym_vel_g-d_strain).in_tens_basis().values
                            ))
    # Last check
    residual = linop.dot(w_vec) - (sym_vel_g-d_strain).in_tens_basis().values
    rel_error = (np.linalg.norm(residual)/sym_vel_g.norm())
    spin = w_vec[0]*b_0 + w_vec[1]*b_1 + w_vec[2]*b_2
    return spin, rel_error

def vel_grad_integrate(vgrad, d_t, dgrad_old, strategy="implicit"):
    """ Integrate the given vgrad over a time increment

    Arguments
    ---------
    vgrad: so.Tensor
        the vgrad tensor to integrate
    d_t : float
        the time increment size
    dgrad_old: so.Tensor
        the previous solution
    strategy : str
        Implicit or explicit

    Notes
    -----
    """
    one = so.Tensor.projector('identity',vgrad.dim, order=2)
    if strategy.lower() == 'explicit':
        transfo = (one + vgrad*d_t)*dgrad_old
    elif strategy.lower() == 'implicit':
        transfo = (one - vgrad*d_t).inverse()*dgrad_old
    return transfo
