"""Materials: elasticity

This module contains classes and utilities connected with elastic behaviors.
Any material class derives from the base class
    somech.material_core.ConstitutiveModel
"""
import os
import sys
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

#pylint: disable=wrong-import-position
from somech import tensors as tsr
from somech import materials_core as mc
Tensor = tsr.Tensor


class LinearElastic(mc.ConstitutiveModel):
    r"""Elastic, linear, reversible: the easy stuff

    Only the isotropic case (2 parameters) is supported for now.
    Linear elasticity is characterized by a 4th order operator such that
        :math:`\sigma = K:\epsilon`

    In tensor basis and dim 3 (build with sqrt2:=r2)
        |   Sig11 | |  | C1111 C1122 C1133 r2.C1123 r2.C1113 r2.C11h2 | |   Eps11 |
        |   Sig22 | |  |       C2222 C2233 r2.C2223 r2.C2213 r2.C2212 | |   Eps22 |
        |   Sig33 | |  |             C3333 r2.C3323 r2.C3313 r2.C3312 | |   Eps33 |
        | r2Sig23 |=|  |                    2.C2323  2.C2313  2.C2312 |*| r2Eps23 |
        | r2Sig13 | |  | sym.                        2.C1313  2.C1312 | | r2Eps13 |
        | r2Sig12 | |  |                                      2.C1212 | | r2Eps12 |

    Attributes
    ----------
    params : dict
        Contains all the parameters indexed by name.
    typename : str
        Purely informative, the type of symmetry.

    Example
    -------
    >>> aluminium = LinearElastic(E=70000, nu=.33)
    >>> aluminium.typename
    'isotropic'
    >>> aluminium.params
    {'E': 70000, 'nu': 0.33}
    >>> aluminium.dim
    3
    """

    # That abstract property:
    var_types = np.dtype([
        ('stress', Tensor),
        ('strain', Tensor),
        ])
    def __init__(self, **params_):
        """Store Young moduli and Poisson coefficients

        For now only the isotropic case (2 params E, nu) is supported.

        Arguments
        ---------
        params_ : dict
            params_['E'] : float
                Young modulus
            params_['nu'] : float
                Poisson coefficient
        """
        # Handle input parameters
        nb_par = len(params_)
        expected_params = {}
        elas_type = 'UNKNOWN'
        # Guess case depending on how many independent components
        if nb_par == 2: # Isotropic, all directions behave the same
            elas_type = 'isotropic'
            expected_params = {'E', 'nu'}
        elif nb_par == 5: # Transverse isotrope
            elas_type = 'transverse isotropic'
        elif nb_par == 9: # Orthotropic, 3 planes (2 gives you the third)
            elas_type = 'orthotropic'
        elif nb_par == 13:# Monoclinic, 1 plane of symmetry
            elas_type = 'monoclinic'
        elif nb_par == 21:# Triclinic, general case, only major and minor symmetries
            elas_type = 'triclinic'

        if elas_type == 'UNKNOWN':
            raise NotImplementedError(
                'Intended to instantiate LinearElastic with '
                +str(nb_par)+' parameters. This is not supported.')

        if expected_params == {}:
            raise NotImplementedError(
                'LinearElastic: after the number of provided parameters ('
                +str(nb_par)+') you seem to expect the type: '
                +elas_type+' . Sorry, not coded yet.'
                )

        if set(params_.keys()) != expected_params:
            raise ValueError(
                'LinearElastic: after the number of provided parameters ('
                +str(nb_par)+') you seem to expect the type: '
                +elas_type+' . '
                +'The expected parameter keys are '+str(expected_params)
                +' but you provided :'+str(set(params_.keys()))+'.'
                )

        self.typename = elas_type
        self._stif = None
        self._comp = None
        self.params = params_

    def solve(self, sols_old, sols_new):
        # Figure out the loading type,
        # "the set of things that are set":
        set_set = set(self._which_set_state(sols_new)) - {'time'}
        # Problem if more than one loading type
        if len(set_set) > 1:
            raise Exception('I dunno what but there has to be a problem')
        # Incremental strain from stress
        if set_set == {'stress'}:
            d_sig = sols_new['stress']-sols_old['stress']
            d_eps = self.compliance() ** d_sig
            sols_new['strain'] = sols_old['strain'] + d_eps
        # Incremental stress from strain
        elif set_set == {'strain'}:
            d_eps = sols_new['strain']-sols_old['strain']
            d_sig = self.stiffness() ** d_eps
            sols_new['stress'] = sols_old['stress'] + d_sig
        else:
            raise Exception('Wrong load type it seems.')
        return sols_new

    def stiffness(self):
        """Return the 4th order elastic stiffness"""
        # If it was computed already {'nu', 'E'} but
        if self._stif is not None:
            return self._stif
        # Else
        if self.typename == 'isotropic':
            p_h = Tensor.projector('hyd', self.dim, order=4)
            p_d = Tensor.projector('dev', self.dim, order=4)
            kelvin_compr = self.params["E"]/3./(1.-2.*self.params["nu"])
            kelvin_shear = self.params["E"]/2./(1.+self.params["nu"])
            self._stif = 3.*kelvin_compr*p_h + 2.*kelvin_shear*p_d
            return self._stif
        # Otherwise
        raise NotImplementedError()

    def compliance(self):
        """Return the 4th order compliance == stiffness inverse"""
        # If it was computed already
        if self._comp is not None:
            return self._comp
        # Else
        if self.typename == 'isotropic':
            p_h = Tensor.projector('hyd', self.dim, order=4)
            p_d = Tensor.projector('dev', self.dim, order=4)
            kelvin_compr = self.params["E"]/3./(1.-2.*self.params["nu"])
            kelvin_shear = self.params["E"]/2./(1.+self.params["nu"])
            self._comp = 1/(3*kelvin_compr)*p_h + 1/(2.*kelvin_shear)*p_d
            return self._comp
        # Otherwise
        raise NotImplementedError()

if __name__ == "__main__":
    import doctest
    doctest.testmod()
