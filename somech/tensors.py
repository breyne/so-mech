# pylint: disable=protected-access
'''Tensors in continuum mechanics

---------------------
Structure and storage
---------------------

Handling different types of tensors
- of R2 and R3 (dimension 2 or 3)
- of order 1 (vectors),
- or 2 (e.g. stresses),
- or 4 (e.g. tangent operators).
- with or without symmetry.

In the general case, we'll use standard **vectorial** bases, for example:
    e_1 = (1 0 0);
    e_2 = (0 1 0);
    e_3 = (0 0 1);

In the symmetric case, we'll make use of symmetric **tensorial** bases, defined as:
    E_1 = e1 x e1 ;     E_4 = sym(e2 x e3)/sqrt(2)
    E_2 = e2 x e2 ;     E_5 = sym(e1 x e3)/sqrt(2)
    E_3 = e3 x e3 ;     E_6 = sym(e1 x e4)/sqrt(2)
Note that the previous convention is carried in the whole module.
In the case of symmetric tensor, this basis allows to enforce symmetry and reduce storage
while conserving tensorial operations like transpose, inverse, contraction, etc.

The interest of this module is to make operations seemless from the user's perspective
regardless of the chosen structure.

The table below summarizes the storage type as numpy.shape.
Note that:
- the symmetric case is irrelevant for vectors.
- The non-sym case is irrelevant for 4th order tensor.
  Okay, maybe not in absolute but don't we mostly have mino symmetries?
- The tensor order can be deduced from its dimension (R2 or R3) and its shape.
  I.e. instanciation only requires an array and a shape.

| order   | R2    | R2-sym | R3    | R3-sym |
| ------- | ----- | ------ | ----- | ------ |
| 1       | (2,)  |  ---   | (3,)  |  ---   |
| 2       | (2,2) | (3,)   | (3,3) | (6)    |
| 4       |  ---  | (3,3)  |  ---  | (6,6)  |

----------
Operations
----------

We want to make use of python magic/dunder methods to avoid lengthy calls.

- Addition Aij + Bij -> A + B
- Simple contraction: Aij Bjk -> A*B
- Double contraction: Cijkl Bkl -> C**B
- Tensor Product: Aij Bkl -> A%B
- Cross product a X b -> a^b (wedge, french style)

- Inverse: A^{-1} -> ~A
- Transpose: A^T -> A.T (numpy style)
- Symmetric part: A.sym
- Skew-symmetric part: A.skew()

- Eigenvalue problem: values and vectors.
- Norm: root of the sum of the squares

The methods below alter the tensor instance.
- Normalize:
- Symmetrize:

---------------
Representations
---------------

Vectors are "points" in R(2,3)

Tensors of order 2 can be represented:
- As triplets of vectors
- As vectors in the eigenbasis (cartesian or Lode)
- As vectors on the deviatoric plane.

Tensors of order 4 are not represented

'''

import copy
import warnings
import numpy as np
from scipy.linalg import polar

TOLERANCE = 1e-15
#pylint: disable=too-many-public-methods
class Tensor:
    """Numpy arrays particularized to continuum mechanics

    Attributes
    ----------
    There is no public attribute.

    Notes
    -----
    Method or data-descriptor property?
        The rule of thumb to know wether a feature is a method or a property is:
        - if it returns a tensor of the same dim and order, then it's a prop;
        - else it's a method.

    """

    def __init__(self, val, dim=None):
        """Default and copy constructors

        The tensor is stored as given. The basis (vectorial or tensorial-symmetric)
        is deduced from the shape, and the dim value if necessary.
        So depending on the user's intention, a (3,3) array can be
        - a 2nd-order Cartesian tensor of dimension 3
        - or a 4th-order Bechterew tensor of dimension 2.
        In the same way, a (3,) array can be a vector of R3 or a 2nd-order tensor of dimension 2.

        Arguments
        ---------
        val : Tensor, ndarray or list
            If val is a tensor, it is **deep** copied (shallow copy is left to the
            assignment operator =).
        dim : int
            Dimension (2 or 3), needed to lift ambiguity in the case of size 3 (see table).

        """

        # We first deal with the copy constructor
        if isinstance(val, Tensor):
            self._values = copy.deepcopy(val._values)
            self._dim = copy.deepcopy(val._dim)
            self._ord = copy.deepcopy(val._ord)

        # We then deal with the default constructor
        elif isinstance(val, (list, np.ndarray)):
            # Ensure data type: nd-array of numbers.
            # Also make them floats by ensuring.
            val = np.array(val)*1.
            # Assign if shape is not ambiguous
            if val.shape in ((2,), (2, 2)):
                self._values = val
                self._dim = 2
                self._ord = len(val.shape)
            elif val.shape in ((6,), (6, 6)):
                self._values = val
                self._dim = 3
                self._ord = 2*len(val.shape)
            elif val.shape in((3,), (3, 3)):
                # Check dim and act accordingly
                if dim == 2:
                    self._values = val
                    self._dim = dim
                    self._ord = 2 if len(val.shape) == 1 else 4
                elif dim == 3:
                    self._values = val
                    self._dim = dim
                    self._ord = 1 if len(val.shape) == 1 else 2
                else:
                    raise Exception('Expecting dimension 2 or 3, but ' +str(dim)+' was given.')
            else:
                raise NotImplementedError(
                    'The input array has an unsupported size: '
                    +str(val.shape)
                    +'.'
                    )
        else:
            raise Exception(
                'Constructor expecting an instance of Tensor or list or nd-array, but type '
                +str(type(val))+' was provided.')

        # If an incorrect dim was provided, let the user know?
        if isinstance(dim, int) and (dim != self._dim):
            warnings.warn(
                'Copy constructor: provided Tensor does not have the provided dim. '
                +'You provided dim='+str(dim)
                +' but dim='+str(self._dim)+' was copied.'
                )

    def __repr__(self):
        """Same as __str__"""
        return str(self)

    def __str__(self):
        """Display values + dimension and order.

        Example
        -------
        >>> print(Tensor([1, 1, 1], 2), 'is different than ', Tensor([1, 1, 1], 3))
        [1. 1. 1.]_(2D,O2) is different than  [1. 1. 1.]_(3D,O1)
        """
        return str(self._values)+'_('+str(self._dim)+'D,O'+str(self._ord)+')'

    @property
    def dim(self):
        """Return dimension"""
        # This is the getter function; the setter is not defined hence not callable
        return self._dim

    @property
    def ord(self):
        """Return order"""
        # This is the getter function; the setter is not defined hence not callable
        return self._ord

    @property
    def values(self):
        """Return values as stored"""
        # This is the getter function; the setter is not defined hence not callable
        return self._values

    def is_basis_tens(self):
        """Check the tensorial basis storage according to the data structure."""
        # See table.
        # The constructor already insures the data structure.
        test2 = self._dim == 2 and len(self._values) == 3
        test3 = self._dim == 3 and len(self._values) == 6
        return bool(test2 or test3)

    def is_basis_vect(self):
        """Return the conjugate of is_basis_tens(self)"""
        return not self.is_basis_tens()

    def vectorize_basis(self):
        """Enforce vectorial basis storage"""
        if self.is_basis_tens():
            self._values = _tens2vect(self._values)

    def tensorize_basis(self):
        """Enforce tensorial basis storage"""
        if self.is_basis_vect():
            self._values = _vect2tens(self._values)

    def in_tens_basis(self):
        """Return a tens-basis instance of the Tensor"""
        temp = Tensor(self)
        temp.tensorize_basis()
        return temp

    def in_vect_basis(self):
        """Return a vect-basis instance of the Tensor"""
        temp = Tensor(self) # copy instance
        temp.vectorize_basis()
        return temp

    #def __call__(self, *args, **kwargs):
    #    """Return the instance and throw a warning"""
    #    #raise NotImplementedError('TODO, maybe some kind of display?')
    #    warnings.warn(
    #        "This is method Tensor.__call__(), and it just returns the object."
    #        +"It is useless."
    #        +"You probably mistook an attribute/property for a method/function")
    #    return self

    def __pos__(self):
        """Return the instance"""
        return self

    def __eq__(self, rhs):
        """Compare Tensor instances by value, even if in different bases"""
        if rhs in (None, np.nan):
            return False
        if not isinstance(rhs, Tensor):
            raise TypeError("Expecting a Tensor on right hand side.")

        # If they are not expressed in the same basis then pull everything back to vectorial
        if self.is_basis_tens() == rhs.is_basis_tens():
            np_lhs = self._values
            np_rhs = rhs._values
        else:
            np_lhs = self._values if self.is_basis_vect() else _tens2vect(self._values)
            np_rhs = rhs._values if rhs.is_basis_vect() else _tens2vect(rhs._values)

        #test_v = bool(np.prod(np_lhs == np_rhs))
        test_v = np.sum((np_lhs-np_rhs)**2) < TOLERANCE
        test_d = self._dim == rhs._dim
        test_o = self._ord == rhs._ord
        return bool(test_v * test_d * test_o)

    def __neg__(self):
        """The opposite of the Tensor"""
        return Tensor(-self._values, self._dim)

    def __add__(self, rhs):
        """Add two Tensors, other types not admitted"""
        # We don't treat the addition of a Tensor with something else,
        # that makes no sense here.

        # Make sure rhs is a Tensor
        if not isinstance(rhs, Tensor):
            raise Exception('Expecting a Tensor instance on the right hand side.')

        # Make sure dim and order are identical
        if (self._dim != rhs._dim) or (self._ord != rhs._ord):
            raise Exception('Added Tensors should have same dimension and order.')

        # If they are stored in the same basis, easy
        if self.is_basis_tens() == rhs.is_basis_tens():
            np_lhs = self._values
            np_rhs = rhs._values

        # Otherwise it means that one of the operands is not inherently symmetric.
        # So we run everything in vectorial basis.
        else:
            np_lhs = self._values if not self.is_basis_tens() else _tens2vect(self._values)
            np_rhs = rhs._values if not rhs.is_basis_tens() else _tens2vect(rhs._values)

        return Tensor(np_lhs + np_rhs, self._dim)

    def __sub__(self, rhs):
        """Return the subtraction of two Tensors"""
        #In fact, add the opposite.
        return self + -rhs

    def prod_inner1(self, rhs):
        """Simple contraction, like numpy.dot

        Arguments
        ---------
        rhs : Tensor or number-like
            Scalar is supported and returns the scaled tensor.
            Tensor only makes sense for Tensors of equal dimension.
            Order 4 is not supported, because never needed. Right?
        """
        # Treat the multiplication with a scalar
        if isinstance(rhs, (float, int)):
            return Tensor(rhs * self._values, self._dim)
        # Throw an exception if not a Tensor
        if not isinstance(rhs, Tensor):
            raise TypeError('Expecting a Tensor/float/int instance on the right hand side.')
        # Refuse order 4
        if self._ord == 4 or rhs._ord == 4:
            raise ValueError('Simple contraction involving fourth order Tensor is not supported.')
        # Make sure the dimension is identical
        if self._dim is not rhs._dim:
            raise ValueError('Tensors should have same dimension.')

        # The tensorial basis is not suited for simple contraction.
        # So we run everything in vectorial basis.
        np_lhs = self._values if not self.is_basis_tens() else _tens2vect(self._values)
        np_rhs = rhs._values if not rhs.is_basis_tens() else _tens2vect(rhs._values)

        # At this point we have two compatible arrays in a vectorial basis.
        np_res = np_lhs.dot(np_rhs)

        # If the result is scalar then return it as such
        if np_res.shape == ():
            return float(np_res)

        # Finally we re-symmetrize the case of two symmetric tensors
        if self.is_basis_tens() and rhs.is_basis_tens():
            return Tensor(_vect2tens(np_res), self._dim)
        # ... or leave it untouched otherwise
        return Tensor(np_res, self._dim)

    def prod_inner2(self, rhs):
        """Return the double-dot product of two Tensors,

        Arguments
        ---------
        rhs : Tensor
            order > 1 otherwise contracting twice doesn't make sense.
        """

        # Throw an exception if not a Tensor
        if not isinstance(rhs, Tensor):
            raise TypeError('Expecting a Tensor instance on the right hand side.')
        # Refuse order 1
        if self._ord == 1 or rhs._ord == 1:
            raise ValueError('Double contraction involving a first order Tensor is impossible.')
        # Make sure the dimension is identical
        if self._dim is not rhs._dim:
            raise ValueError('Tensors should have same dimension.')

        # This time symmetric bases have precedence because
        # the skew and sym parts don't mix up during double-contraction.
        # So if one is symmetric, we run everything in tensorial basis.
        if self.is_basis_tens() or rhs.is_basis_tens():
            np_lhs = self._values if self.is_basis_tens() else _vect2tens(self._values)
            np_rhs = rhs._values if rhs.is_basis_tens() else _vect2tens(rhs._values)
            np_res = np_lhs.dot(np_rhs)
        # Else both are in vectorial basis : we use tensordot on 2 axes to contract twice
        else:
            np_res = np.tensordot(self._values, rhs._values, axes=2)

        # If the result is scalar then return it as such
        if np_res.shape == ():
            return float(np_res)

        # Otherwise return the as such
        return Tensor(np_res, self._dim)

    def prod_cross(self, rhs):
        """Cross product of two vectors (ord=1) of dimension 3"""
        # Throw an exception if not a Tensor
        if not isinstance(rhs, Tensor):
            raise TypeError('Expecting a Tensor instance on the right hand side.')
        # Only accept order 1
        if self._ord != 1 or rhs._ord != 1:
            raise ValueError('Cross product is only supported for two vectors')
        # Enforce dim
        if self._dim != 3 or rhs._dim != 3:
            raise ValueError('Cross product does not make sense in dimension 2!')

        np_lhs = self._values
        np_rhs = rhs._values
        return Tensor(np.cross(np_lhs, np_rhs), self._dim)

    def prod_outer(self, rhs):
        """Outer product or tensorial product

        Notes
        -----
        Only supports Tensors of equal dimension and order and basis.
        """
        # Throw an exception if not a Tensor
        if not isinstance(rhs, Tensor):
            raise TypeError('Expecting a Tensor instance on the right hand side.')

        # This time vectorial bases have precedence.
        # So if one is vectorial, we run everything in vectorial basis.
        if self.is_basis_vect() or rhs.is_basis_vect():
            np_lhs = self._values if self.is_basis_vect() else _tens2vect(self._values)
            np_rhs = rhs._values if rhs.is_basis_vect() else _tens2vect(rhs._values)
        # Else both are in tensorial basis
        else:
            np_lhs = self._values
            np_rhs = rhs._values

        # Else both are in vectorial basis : we use tensordot on 0 axes
        np_res = np.tensordot(np_lhs, np_rhs, axes=0)
        return Tensor(np_res, self._dim)

    def inverse(self):
        """Return the inverse tensor"""
        # Refuse order 1
        if self._ord == 1:
            raise ValueError('Wait, what is the inverse of a vector?')
        # Use vectorial representation if tensorial of order 2
        if self.is_basis_tens() and self._ord == 2:
            inv = np.linalg.inv(_tens2vect(self._values))
            return Tensor(_vect2tens(inv), self._dim)
        # Straightforward otherwise
        inv = np.linalg.inv(self._values)
        return Tensor(inv, self._dim)

    def __truediv__(self, rhs):
        """Operator /: divide a Tensor with a scalar"""
        # That is multiply by the inverse of that scalar.
        return self.prod_inner1(1./rhs)

    def __mul__(self, rhs):
        """Operator *: wrapper for `prod_inner1` (simple contraction)"""
        return self.prod_inner1(rhs)

    def __rmul__(self, lhs):
        """Operator *: wrapper for `prod_inner1` (simple contraction)"""
        return self.prod_inner1(lhs)

    def __pow__(self, rhs):
        """Operator **: wrapper for `prod_inner2` (double contraction)"""
        if isinstance(rhs, (int)):
            raise NotImplementedError('Is it really useful?')
        return self.prod_inner2(rhs)

    def __mod__(self, rhs):
        """Operator %: wrapper for `prod_outer` (tensor product).
        I know this is sacrilegious, sorry."""
        return self.prod_outer(rhs)

    def __xor__(self, rhs):
        """Operator ^: wrapper for `prod_cross`. I know this is sacrilegious, sorry."""
        return self.prod_cross(rhs)

    def __invert__(self):
        """Operator ~: wrapper for `inverse`"""
        return self.inverse()

    @property
    def T(self):#pylint: disable=invalid-name
        """Return the transpose of a tensor"""
        # Refuse order 1
        if self._ord == 1:
            raise ValueError('Wait, what is the transpose of a vector?')
        # No need to change basis here
        return Tensor(self._values.T, self._dim)

    @property
    def sym(self):
        """Return the symmetric part of an even order Tensor instance"""
        if self._ord in (2, 4):
            return .5*(self+self.T)
        raise ValueError('Wrong tensorial order for symmetric part.')

    @property
    def skew(self):
        """Return the skew-symmetric part of an even order Tensor instance"""
        if self._ord in (2, 4):
            return .5*(self-self.T)
        raise ValueError('Wrong tensorial order for skew-symmetric part.')

    @property
    def hyd(self):
        """Return the hydrostatic part of a 2nd order Tensor instance"""
        if self._ord == 2:
            return self ** Tensor.projector('hyd', self._dim)
        raise ValueError('Only defined for 2nd order tensor.')

    @property
    def dev(self):
        """Return the deviatoric part of a 2nd order Tensor instance"""
        if self._ord == 2:
            return self ** Tensor.projector('dev', self._dim)
        raise ValueError('Only defined for 2nd order tensor.')

    def norm(self):
        """Frobenius norm: root of the sum of the squares."""
        squares = self._values * self._values
        return np.sqrt(np.sum(squares))

    @property
    def normed(self):
        """Return a normalized tensor"""
        return self/self.norm()

    def trace(self):
        """Return the trace of a 2nd order tensor"""
        if self._ord != 2:
            raise ValueError("Trace is only supported for order 2")
        # We must use vectorial bases for np.linalg.det
        np_arr = self._values if self.is_basis_vect() else _tens2vect(self._values)
        return np_arr.trace()

    def det(self):
        """Return the determinant of a 2nd order tensor ... and 4th?"""

        if self._ord not in (2, 4):
            raise ValueError("Determinant is only supported for order 2")

        # We must use vectorial bases for np.linalg.det at order 2
        if self._ord == 2:
            np_arr = self._values if self.is_basis_vect() else _tens2vect(self._values)

        # The latter is defined at least for the sake of unit testing.
        # I don't know what it means for order 4
        if self._ord == 4:
            np_arr = self._values

        return np.linalg.det(np_arr)

    def invariant(self, name):
        """Rivlin-Ericksen (Jk) or Cayley-Hamilton (Ik), 1<=k<=3

        These are only defined for symmetric tensors

        Arguments
        ---------
        name : str
            Expected values are J1, J2, J3, I1, I2, I3.
            (not case-sensitive)

        """
        # Ensure that the arguments name and dim
        if not isinstance(name, str):
            raise TypeError('Expecting a string for argument *name*.')
        # This should throw a warning if the tensor is not sym:
        tens = self.in_tens_basis()
        # Rivlin-Ericksen
        if name.lower() == 'j1':
            return tens.trace()
        if name.lower() == 'j2':
            return (tens*tens).trace()/2.
        if name.lower() == 'j3':
            #pdb.set_trace
            return (tens*tens*tens).trace()/3.
        # Rivlin-Ericksen
        if name.lower() == 'i1':
            return tens.trace()
        if name.lower() == 'i2':
            return (tens.trace()**2 - (tens*tens).trace())/2.
        if name.lower() == 'i3':
            return tens.det()
        raise ValueError('The provided string is not supported')

    def decomposition_eigen(self):
        """Return eigenvalues and eigendirections

        This uses np.linalg.eig() on the appropriate array representation.
        Note that it also makes sense to seek the eigentensors associated
        with a 4th order tensor.

        Returns
        -------
        vals : tuple of floats
            Ordered eigenvalues.
        vecs : tuple of Tensors
            Corresponding eigentensors of order self.ord/2
        """
        # Ensure order 2 or 4
        if self._ord not in (2, 4):
            raise ValueError("Eigenvalues: tensor must be of order 2 or 4")
        if self._ord == 2:
            np_arr = self.in_vect_basis()._values
        else:
            np_arr = self._values
        np_vals, np_vecs = np.linalg.eig(np_arr)

        # Now let us build tuples to return
        vals = ()
        vecs = ()
        for dim, eigvec in enumerate(np_vecs.T):
            vals += (np_vals[dim],)
            vecs += (Tensor(eigvec, self._dim),)
        return vals, vecs

    def decomposition_polar(self, side='right'):
        """Return the polar decomposition

        This uses scipy.linalg.polar() on the appropriate array
        representation. The decomposition can be written
            Fia = Rij Uja (side='right')
                = Vij Rja (side='left')
        where R is orthogonal and U/V is symmetric.

        Returns
        -------
        R : Tensor
            The orthogonal rotation, identical in both cases.
        U or V : Tensor
            The dilatation part, depending on the value of `side`.

        """
        # Ensure order 2
        if self._ord != 2:
            raise TypeError(
                "Polar decomposition is only supported for 2nd order Tensors. "
                +"Does it make sense otherwise?"
                )

        # Ensure Vectorial basis
        if self.is_basis_tens():
            raise TypeError("Polar decomposition is irrelevant for symmetric things.")

        # Use polar and return
        rot, dilat = polar(self._values, side)
        return Tensor(rot, self._dim), Tensor(dilat, self._dim)

    def decomposition_basis(self):
        """Return the basis decomposition of the given instance

        Any Tensor instance can be written in the form
            T = t_i b_i
        where t_i is an independent component and b_i a tensorial direction.

        Returns
        -------
        vals : tuple of float
            Litterally the tensor's components as stored.
        tens : tuple of Tensor
            Litterally arrays with zeros everywhere except at the corresponding
            components spot where there is a one.
        """
        # Inich'
        vals = ()
        tens = ()

        # Loop on components
        for idx, val in np.ndenumerate(self._values):
            # The basis matrix is a lot of zeros with a one somewhere
            basis = np.zeros(self._values.shape)
            basis[idx] = 1.
            # Append list
            vals += (val,)
            tens += (Tensor(basis, self._dim),)
        return vals, tens

    @property
    def log(self):
        """Return the natural lgarithm of th tensor"""
        vals, vecs = self.decomposition_eigen()
        lnt = np.log(vals[0])*vecs[0].prod_outer(vecs[0]) \
            + np.log(vals[1])*vecs[1].prod_outer(vecs[1]) \
            + np.log(vals[2])*vecs[2].prod_outer(vecs[2])
        return lnt

    @staticmethod
    def projector(name, dim, **kwargs):
        """Generate classical projectors: identity, deviator and spheric

        Arguments
        ---------
        name : str
            The projector's name, that must look like (not case-sensitive):
                - identity or unity      for 2nd or 4th order 1.
                - deviatoric or shear    for 4th order eponym tensor
                - hydrostatic or spheric for 4th order eponym tensor

        dim : int (2 or 3)

        order=value (kwargs) : int
            Must be provided in case of ambiguity

        Notes
        -----

        The Tensor instances are generated in their "natural basis".
        In order to ensure an appropriate storage,
        dont forget to append the appropriate instruction:
            projector(...).in_vect_basis(),  or
            projector(...).in_tens_basis()

        """

        # Set default
        kwargs.setdefault("order", None)

        # Ensure that the arguments name and dim
        if not isinstance(name, str):
            raise TypeError('Expecting a string for argument *name*.')
        # Ensure dim makes sense
        if not dim in (2, 3):
            raise ValueError('Expecting 2 or 3 for argument *dim*.')

        # This function will check if the provided name starts like our keywords
        name_test = lambda KW: KW.startswith(name.lower())
        # If it matches more then once we will throw an exception
        matched = 0

        if name_test('identity') or name_test('unity'):
            matched += 1
            if kwargs["order"] == 2:
                size = 2 if dim == 2 else 3
            elif kwargs["order"] == 4:
                size = 3 if dim == 2 else 6
            else:
                raise ValueError('Argument *order* is either absent or wrong.')
            np_res = np.eye(size)
        if name_test('hydrostatic') or name_test('spheric'):
            matched += 1
            size = 3 if dim == 2 else 6
            np_res = np.zeros([size]*2)
            np_res[0:dim, 0:dim] = 1/dim
        if name_test('deviatoric') or name_test('shear'):
            matched += 1
            id4 = Tensor.projector('Ident', dim, order=4).__dict__['_values']
            hyd = Tensor.projector('Hydro', dim, order=4).__dict__['_values']
            np_res = id4 - hyd

        # Sanctions if wrong name!
        if matched == 0:
            raise ValueError('Unsupported projector name.')
        if matched > 1:
            raise ValueError('Ambiguous projector name: it matched '+str(matched)+' times.')

        # All clear
        return Tensor(np_res, dim)

    @staticmethod
    def eigendeviator2polar(vals):
        """Return the angle in the deviatoric plane (0 rad is s_2=0, and pi/2 is s2=-2s1=-2s3).

        Arguments
        ---------
        vals : tuple of floats
            Typically returned as
            vals, vecs = Tensor.decomposition_eigen()

        Notes
        -----
        The argument is not a Tensor because the eigenvalues sorting behavior is undefined.
        So users has to do it themselves.
        """
        coord_x = (vals[0] - vals[2])*np.sqrt(3)/2#np.cos(np.pi/6)
        coord_y = vals[1] - (vals[0] + vals[2])*.5#np.sin(np.pi/6)
        radius = np.sqrt(coord_x**2 + coord_y**2)
        angle = np.arctan2(coord_y, coord_x)
        return radius, angle

    @staticmethod
    def angle2deviator(ang):
        """Return a deviatoric plane tensor given an angle in rad
        (0 rad is s_2=0, and pi/2 is s2=-2s1=-2s3)."""
        dev_1 = np.sqrt(2/3)*np.cos(np.pi/6+ang)
        dev_2 = np.sqrt(2/3)*np.sin(ang)
        dev_3 = -dev_1-dev_2
        return Tensor([dev_1, dev_2, dev_3, 0, 0, 0])

    @staticmethod
    def gen_deviatoric_unit_circle(amount=10):
        """Yield different samplings of different space over which we can iterate"""
        angles = np.linspace(0, 2*np.pi, amount)
        for ang in angles:
            yield ang, Tensor.angle2deviator(ang)

    @staticmethod
    def gen_planar_unit_circle(amount=10, components=None):
        """Yield different samplings of different space over which we can iterate"""
        components = '1122' if not components else components
        tup_x = np.zeros([3, 3])
        tup_y = np.zeros([3, 3])
        tup_x[(int(components[0])-1, int(components[1])-1)] = 1
        tup_y[(int(components[2])-1, int(components[3])-1)] = 1
        # Turn into Tensors
        ten_x = Tensor(tup_x, 3).normed.in_tens_basis()
        ten_y = Tensor(tup_y, 3).normed.in_tens_basis()
        angles = np.linspace(0, 2*np.pi, amount)
        for ang in angles:
            yield ang, np.cos(ang)*ten_x + np.sin(ang)*ten_y

    @staticmethod
    def gen_planar_unit_circle_1122(amount=10, alt_12=0.):
        """Yield different samplings of different space over which we can iterate"""
        ten_11 = Tensor([1,0,0,0,0,0], 3)
        ten_22 = Tensor([0,1,0,0,0,0], 3)
        ten_12 = Tensor([0,0,0,0,0,1], 3)/np.sqrt(.5)
        angles = np.linspace(0, 2*np.pi, amount)
        for ang in angles:
            yield ang, np.cos(ang)*ten_11 \
                + np.sin(ang)*ten_22 \
                + alt_12*ten_12


def _vect2tens(vec):
    """Turn vectorial representation into tensorial-symmetric

    Arguments
    ---------
    vec : ndarray of shape (2,2) or (3,3)
        A tensor in Bechterew basis, order 2 or 4, dimension 1, 2 or 3.

    Yields
    ------
    tens: ndarray of shape (3,) or (6.)
        I.e. the corresponding tensorial-symmetric representation
    """

    root2 = np.sqrt(2.)

    # Only the 2nd order tensor is treated
    if vec.shape == (2, 2):
        tens = np.zeros(3)
        tens[0] = vec[0, 0]
        tens[1] = vec[1, 1]
        tens[2] = root2 * (vec[1, 0] + vec[0, 1])/2.
        if  abs(vec[1, 0] - vec[0, 1]) > TOLERANCE:
            warnings.warn("The array should have been symmetric.")
    elif vec.shape == (3, 3):
        tens = np.zeros(6)
        tens[0] = vec[0, 0]
        tens[1] = vec[1, 1]
        tens[2] = vec[2, 2]
        tens[3] = root2 * (vec[1, 2] + vec[2, 1])/2.
        tens[4] = root2 * (vec[0, 2] + vec[2, 0])/2.
        tens[5] = root2 * (vec[0, 1] + vec[1, 0])/2.
        norm = np.sqrt(np.sum(tens*tens))
        test1 = abs(vec[1, 2] - vec[2, 1])/norm > TOLERANCE
        test2 = abs(vec[0, 2] - vec[2, 0])/norm > TOLERANCE
        test3 = abs(vec[0, 1] - vec[1, 0])/norm > TOLERANCE
        if test1 or test2 or test3:
            warnings.warn("The array should have been symmetric.")
    else:
        raise NotImplementedError('Unsupported array shape.')

    return tens

def _tens2vect(tens):
    """Turn tensorial-symmetric representation into vectorial

    Arguments
    ---------
    tens: ndarray
        A tensor in Bechterew basis, order 2 or 4, dimension 1, 2 or 3.

    Yields
    ------
    vec : ndarray
        As many axes as tensorial orders, with the right values.
    """
    # Find dimension
    if   len(tens) == 6:
        dim = 3
    elif len(tens) == 3:
        dim = 2
    elif len(tens) == 1:
        return tens
    else:
        raise ValueError("Unsupported input.")

    # Try order 4
    if len(tens.shape) == 2:
        vec = np.zeros([dim, dim, dim, dim])
        raise NotImplementedError("Only with order 2 for now, sorry.")
    # Else the order is 2
    if len(tens.shape) == 1:
        root2 = np.sqrt(2.)
        vec = np.zeros([dim, dim])
        # Assign diagonal terms
        for i, value in enumerate(tens[0:dim]):
            vec[i, i] = value
        # Assign non diagonal terms
        if dim == 3:
            vec[[1, 2], [2, 1]] = tens[3]/root2
            vec[[0, 2], [2, 0]] = tens[4]/root2
            vec[[0, 1], [1, 0]] = tens[5]/root2
        else: #dim==2:
            vec[[0, 1], [1, 0]] = tens[2]/root2
        return vec
    # Throw an exception if the shape is wrong
    raise ValueError("Unsupported input shape.")

# If we want to import tensors as ten and then use ten.Sor
Sor = Tensor

one_d3o2 = Tensor.projector('identity', 3, order=2)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
