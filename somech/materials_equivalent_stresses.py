"""This module defines the EquivalentStress class and several instances.

An equivalent stress is a scalar representing a second order tensor
and used in constitutive models.
"""

import os
import sys
import numpy as np
from scipy.optimize import root_scalar

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from somech import tensors as sote#pylint: disable=wrong-import-position
from somech import materials_core as mx#pylint: disable=wrong-import-position
Tensor = sote.Tensor

class EquivalentStress(mx.ParametricFunction):
    """The scalar that represents the stress Tensor

    New instances can be declared but a number of classical norms are built-in.
    See: EquivalentStress.<mises, tresca, hill48>
    The instances are callable:
        self(x) returns self.function(x, **self.parameters)

    Attributes
    ----------
    function : callable
        function(Tensor, **parameters) -> float
    function_derivative: callable
        function(Tensor, **parameters) -> Tensor
    parameters : dict
        Some models require parameters p1, ..., pn:
        parameters = {"p1":value1, ..., "p1":valueN}

    Example
    -------
    >>> stress = 100*Tensor([1, 2, 3, 0, 0, .5])
    >>> vonMises = EquivalentStress.mises()
    >>> sigma_eq = vonMises(stress)
    >>> gradient = vonMises.derivative(stress)
    """

    def __init__(self, fun_, deriv_=None, **params):
        """Initialize parametric function

        Arguments
        ---------
        fun_ : callable(stress: Tensor, **param) -> float
            stress is a tensor
        deriv_ : callable(stress: Tensor, **param) -> Tensor
        params : dict
            Optional default parameters
        """
        # This will pass fun_ as attribute and initialize the parameters
        super(EquivalentStress, self).__init__(fun_, **params)
        # The added thing here is that we want to define a derivative function
        self.function_derivative = deriv_

    def derivative(self, stress):
        """Return the derivative of the equivalent stress function.

        Numerical derivative by default.
        Can be redefined to provide an analytical form.
        """
        if self.function_derivative is None:
            return self.derivative_numerical(stress)
        if callable(self.function_derivative):
            return self.function_derivative(stress, **self.parameters)
        raise TypeError('Class EquivalentStress: `function_derivative` should be a callable.')

    def derivative_numerical(self, stress):
        """Return the derivative of a scalar-valued function of a second order tensor."""
        diff_len = 1E-3
        deriv = stress*0.
        _, tens = stress.decomposition_basis()
        for ten in tens:
            f_plus = self(stress+.5*diff_len*ten)
            f_minus = self(stress-.5*diff_len*ten)
            deriv = deriv + ten * (f_plus-f_minus)/diff_len
        return deriv

    def updated_variables(self, *args, **parameters):#pylint: disable=unused-argument,no-self-use
        """Return updated parameters"""
        return parameters

    @staticmethod
    def mises():
        """f_mises = ||dev(sigma)|| * np.sqrt(3./2.) = sqrt(sigma : P_D : sigma) * np.sqrt(3./2.)

        The isotropic function defined as the norm of the deviatoric part
        where the quadratic form uses the deviatoric projector:
                  |  2  -1  -1             |
                  |      2  -1             |
                  |          2             |
            P_D = |              3         | * 1/3.
                  | sym.             3     |
                  |                      3 |

        """
        def function_(cauchy, **p):#pylint: disable=unused-argument
            """Return the norm of the deviatoric part of a tensor"""
            return cauchy.dev.norm() * np.sqrt(3./2.)
        def derivative_(cauchy, **p):#pylint: disable=unused-argument
            """ Return the normed tensor"""
            deviator = cauchy.dev
            return deviator.normed * np.sqrt(3./2.)
        obj = EquivalentStress(function_, derivative_)
        obj.find_value = obj.find_value_homogeneous
        return obj

    @staticmethod
    def tresca():
        """f_tresca = sup|sigma_i - sigma_j|/2

        The maximum shear component.
        """

        def function_(cauchy, **p):#pylint: disable=unused-argument
            """Return the maximal difference between the eigenvalues of a tensor"""
            vals, _ = cauchy.decomposition_eigen()
            max_shear = 0.
            for i, ai_ in enumerate(vals):
                for aj_ in vals[i:]:
                    max_shear = max(max_shear, abs(ai_-aj_))
            return max_shear
        obj = EquivalentStress(function_)
        obj.find_value = obj.find_value_homogeneous
        return obj

    @staticmethod
    def hill48(**p):
        """Hill 1948: anisotropic norm with 6 parameters.

        The expression reads:
            f_hill = sqrt(sigma : H : sigma)

        The anisotropic function with the quadratic operator:
                |  G+H   -H   -G   0  0  0 |
                |       F+H   -F   0  0  0 |
            H = |            F+G   0  0  0 |
                |                 2L  0  0 |
                |  sym.              2M  0 |
                |                       2N |
        Note that when
        - F=G=H=1/2
        - and L=M=N=3/2
        then we retrieve the deviatoric projector and von Mises (up to a factor)
        """
        # set default parameters
        p.setdefault('F', 1.)
        p.setdefault('H', 1.)
        p.setdefault('G', 1.)
        p.setdefault('L', 1.)
        p.setdefault('M', 1.)
        p.setdefault('N', 1.)

        # Define function
        def function_(cau, **p):
            # Build Hill's tensorial basis
            e_h = Tensor([1, -1, 0, 0, 0, 0])
            e_f = Tensor([0, 1, -1, 0, 0, 0])
            e_g = Tensor([1, 0, -1, 0, 0, 0])
            e_l = Tensor([0, 0, 0, 1, 0, 0])
            e_m = Tensor([0, 0, 0, 0, 1, 0])
            e_n = Tensor([0, 0, 0, 0, 0, 1])
            # Generate the multilinear form
            basis = np.array([e_h%e_h, e_f%e_f, e_g%e_g, e_l%e_l, e_m%e_m, e_n%e_n])
            coeffs = np.array([p['H'], p['F'], p['G'], 2*p['L'], 2*p['M'], 2*p['N']])
            # \sum_i coeff_i * e_i % e_i reads:
            hill = basis.dot(coeffs)
            return np.sqrt(cau**hill**cau)

        # Return neat instance
        obj = EquivalentStress(function_, **p)
        obj.find_value = obj.find_value_homogeneous
        return obj

    @staticmethod
    def yld2000_2d(**p):
        r"""Barlat yld2000-2D [1]: anisotropic, plane stress only

        f_yld =( |   X`_1 -   X`_2|^a  / 2
               + |2*X``_1 -   X``_2|^a / 2
               + |  X``_1 - 2*X``_2|^a / 2 )^(1/a)

        where X`  = L` :\sigma
              X`` = L``:\sigma

        with the 4th order operators L` and L`` in plane stress:

                | L11  L12   0 |             |sig11
            L ~ | L21  L22   0 | for sigma ~ |sig22
                |   0    0 L66 |             |sig12

        expressed in the parametric form

            / L`11 \   / 2/3   0    0 \
            | L`12 |   |-1/3   0    0 | / a1 \
            | L`21 | = |  0  -1/3   0 | | a2 |
            | L`22 |   |  0   2/3   0 | \ a7 /
            \ L`66 /   \  0    0    1 /

            / L``11 \   /-2  2  8 -2  0 \ / a3 \
            | L``12 |   | 1 -4 -4  4  0 | | a4 |
            | L``21 | = | 4 -4 -4  1  0 | | a5 | * (1/9)
            | L``22 |   |-2  8  2 -2  0 | | a6 |
            \ L``66 /   \ 0  0  0  0  9 / \ a8 /

        References
        ----------
        .. [1] Barlat et al., 2003: "Plane stress yield function for
            aluminum alloy sheets—part 1: theory"
            International Journal of Plasticity.
            doi.org/10.1016/S0749-6419(02)00019-0
        """
        # set default parameters
        #p={ 'a': 2., 'α1': 1., 'α2': 1., 'α3': 1., 'α4': 1., 'α5': 1., 'α6': 1., 'α7': 1., 'α8': 1.,}
        p.setdefault('a', 2.)
        p.setdefault('α1', 1.)
        p.setdefault('α2', 1.)
        p.setdefault('α3', 1.)
        p.setdefault('α4', 1.)
        p.setdefault('α5', 1.)
        p.setdefault('α6', 1.)
        p.setdefault('α7', 1.)
        p.setdefault('α8', 1.)

        # Define function
        def function_(cau, **p):
            # Initialize projectors
            tmp = np.array([
                [ 2/3,   0 , 0],
                [-1/3,   0 , 0],
                [  0 , -1/3, 0],
                [  0 ,  2/3, 0],
                [  0 ,   0 , 1],
                ]).dot(np.array([[
                    p['α1'],
                    p['α2'],
                    p['α7'],
                    ]]).T
                )
            transfo_a = Tensor([
                [tmp[0,0], tmp[1,0],      0  ],
                [tmp[2,0], tmp[3,0],      0  ],
                [   0    ,      0  , tmp[4,0]],
                ], 2)
            tmp = 1/9*np.array([
                [-2.,  2,  8, -2,  0,],
                [ 1., -4, -4,  4,  0,],
                [ 4., -4, -4,  1,  0,],
                [-2.,  8,  2, -2,  0,],
                [ 0.,  0,  0,  0,  9,],
                ]).dot(np.array([[
                    p['α3'],
                    p['α4'],
                    p['α5'],
                    p['α6'],
                    p['α8'],
                    ]]).T
                )
            transfo_b = Tensor([
                [tmp[0,0], tmp[1,0],      0  ],
                [tmp[2,0], tmp[3,0],      0  ],
                [   0    ,      0  , tmp[4,0]],
                ], 2)

            # make sure cau is in dimension 2
            cau_ = None
            #cau = so.Tensor([1,2,0], 2)
            if cau.dim == 2:
                cau_ = cau
            elif cau.dim == 3:
                tmp = cau.in_tens_basis().values
                cau_ = Tensor( [tmp[0], tmp[1], tmp[5]/np.sqrt(2), ] , 2)

            x_a = transfo_a**cau_
            xa_xx = x_a.values[0]
            xa_yy = x_a.values[1]
            xa_xy = x_a.values[2]
            va1 = (xa_xx+xa_yy + np.sqrt((xa_xx-xa_yy)**2+4*xa_xy**2))/2
            va2 = (xa_xx+xa_yy - np.sqrt((xa_xx-xa_yy)**2+4*xa_xy**2))/2

            x_b = transfo_b**cau_
            xb_xx = x_b.values[0]
            xb_yy = x_b.values[1]
            xb_xy = x_b.values[2]
            vb1 = (xb_xx+xb_yy + np.sqrt((xb_xx-xb_yy)**2+4*xb_xy**2))/2
            vb2 = (xb_xx+xb_yy - np.sqrt((xb_xx-xb_yy)**2+4*xb_xy**2))/2

            #import pdb; pdb.set_trace()

            # Compute function

            return (np.abs(  va1 -  va2)**p['a']/2 +
                    np.abs(2*vb1 +  vb2)**p['a']/2 +
                    np.abs(  vb1 +2*vb2)**p['a']/2
                   )**(1/p['a'])

        # Return neat instance
        obj = EquivalentStress(function_, **p)
        obj.find_value = obj.find_value_homogeneous
        return obj

    @staticmethod
    def yld2004(**p):
        r"""Barlat yld2004 [1]: anisotropic, 18 parameters

        f_yld = (1/4*\sum_{i,j} |s`_i - s``_j|^a )^(1/a)

        where s`  = C` :\dev(\sigma)
              s`` = C``:\dev(\sigma)

        with the 4th order operators C` and C`` of the parametric form:

                |    0  -c12  -c13    0    0    0 |
                | -c21     0  -c23    0    0    0 |
            C ~ | -c31  -c32     0    0    0    0 |
                |    0     0     0  c44    0    0 |
                |    0     0     0    0  c55    0 |
                |    0     0     0    0    0  c66 |

        References
        ----------
        .. [1]  Barlat et. al., 2005: "Linear transfomation-based
            anisotropic yield functions".
            International Journal of Plasticity.
            doi.org/10.1016/j.ijplas.2004.06.004
        """
        # set default parameters
        p.setdefault('a', 2.)
        # Upper triangles
        p.setdefault('c`12', 1.)
        p.setdefault('c`13', 1.)
        p.setdefault('c`23', 1.)
        p.setdefault('c``12', 0.)
        p.setdefault('c``13', 0.)
        p.setdefault('c``23', 0.)
        # Lower triangles
        p.setdefault('c`21', 1.)
        p.setdefault('c`31', 1.)
        p.setdefault('c`32', 1.)
        p.setdefault('c``21', 0.)
        p.setdefault('c``31', 0.)
        p.setdefault('c``32', 0.)
        # Diagonals
        p.setdefault('c`44', 1.)
        p.setdefault('c`55', 1.)
        p.setdefault('c`66', 1.)
        p.setdefault('c``44', 0.)
        p.setdefault('c``55', 0.)
        p.setdefault('c``66', 0.)

        # Define function
        def function_(cau, **p):
            transfo_a = Tensor([
                [         0, -p['c`12'], -p['c`13'],         0,         0,         0],
                [-p['c`21'],          0, -p['c`23'],         0,         0,         0],
                [-p['c`31'], -p['c`32'],          0,         0,         0,         0],
                [         0,          0,          0, p['c`44'],         0,         0],
                [         0,          0,          0,         0, p['c`55'],         0],
                [         0,          0,          0,         0,         0, p['c`66']]
                ])
            transfo_b = Tensor([
                [          0, -p['c``12'], -p['c``13'],          0,          0,         0],
                [-p['c``21'],           0, -p['c``23'],          0,          0,         0],
                [-p['c``31'], -p['c``32'],           0,          0,          0,         0],
                [          0,           0,           0, p['c``44'],          0,         0],
                [          0,           0,           0,          0, p['c``55'],         0],
                [          0,           0,           0,          0,          0, p['c``66']]
                ])
            #import pdb; pdb.set_trace()
            vals_a, _ = (transfo_a**cau.dev).decomposition_eigen()
            vals_b, _ = (transfo_b**cau.dev).decomposition_eigen()
            my_sum = 0.
            for sa_ in vals_a:
                for sb_ in  vals_b:
                    my_sum += np.abs(sa_-sb_)**p['a']
            return (my_sum/4.)**(1./p['a'])

        # Return neat instance
        obj = EquivalentStress(function_, **p)
        obj.find_value = obj.find_value_homogeneous
        return obj

    @staticmethod
    def kinematic_hardening(**p):
        """Regular kinmatic hardening"""
        p.setdefault('eq_stress', EquivalentStress.mises())
        p.setdefault('X', Tensor([0, 0, 0, 0, 0, 0]))
        def function_(cau, **p):
            return p['eq_stress'](cau- p['X'])
        obj = EquivalentStress(function_, **p)
        #obj.find_value = obj.find_value_homogeneous
        return obj

    @staticmethod
    def hah2020(**par):#pylint: disable=too-many-statements
        """Barlat HAH latest version [1]

        f_hah20  = | phi_1**q + phi_2**q |**(1/q)
            where:
            phi_1    = | f_ref(s_L)**p f_ref(s_X)**p  |**(1/p)
            phi_2**q = f_+^q<h:s>_+^q + f_-^q<h:s>_-^q

        s_L = P_perp/g_L + P_para / (xi_L*(g_L-1)+1)
        s_X = P_perp * xi_C*(1-g_C)/g_L

        P_para = h x h
        P_perp = I - P_para


        References
        ----------
        .. [1] Barlat et al, 2020 : "Distortional plasticity framework
            with application to advanced high strength steel".
            International Journal of Plasticity.
        """
        # The default equivalent stress
        par.setdefault('eq_stress', EquivalentStress.mises())
        # State variables
        par.setdefault('h', Tensor([0, 0, 0, 0, 0, 0]))
        par.setdefault('h^s', Tensor([0, 0, 0, 0, 0, 0]))# starred
        par.setdefault('h^p', Tensor([0, 0, 0, 0, 0, 0]))# prime
        par.setdefault('g_L', 1.)
        par.setdefault('g_C', 1.)
        par.setdefault('f+', 0.)
        par.setdefault('f-', 0.)
        par.setdefault('g+', 1.)
        par.setdefault('g-', 1.)
        par.setdefault('g_P', 1.)
        par.setdefault('g_P^s', 1.)# starred
        par.setdefault('g_3', 1.)
        par.setdefault('g_3^p', 1.)# primed
        par.setdefault('g_3^s', 1.)# starred
        par.setdefault('g_4', 1)
        par.setdefault('g_S', 1.)
        # Genuine parameters
        par.setdefault('hardrule', lambda eps: 1) #/!!!\ don't forget to set it!
        par.setdefault('q', 3.) # ----------
        par.setdefault('p', 3.)
        par.setdefault('k', 150.) # --------
        par.setdefault('k^p', 75.)
        par.setdefault('xi_R', 8.)
        par.setdefault('k_1', 150.) # ------
        par.setdefault('k_2', 75.)
        par.setdefault('k_3', .3)
        par.setdefault('xi_B', 1.0)
        par.setdefault('xi_B^p', 1.0)
        par.setdefault('k_4', .9) # --------
        par.setdefault('k_5', 15.)
        par.setdefault('k_S', 150.)
        par.setdefault('xi_S', 3.)
        par.setdefault('C', .7) # ----------
        par.setdefault('k_C', 60.)
        par.setdefault('k_C^p', 80.)
        par.setdefault('xi_C', 4.)
        par.setdefault('xi_C^p', 1.)
        par.setdefault('k_L', 60.) # -------
        par.setdefault('L', 1.5)
        par.setdefault('xi_L', .5)
        par.setdefault('xi_L^p', .25)
        par.setdefault('MULTI', False)

        # Ensure normed
        if par['h'].norm() > 0.:
            par['h'] = par['h'].normed

        def function_(cau, **par):
            # Identity projector
            iden_4 = Tensor.projector('iden', 3, order=4)
            # Parallel projector
            proj_h = par['h'] % par['h']
            # Perpendicular projector
            perp_h = iden_4 - proj_h
            # Weighted stresses
            stress_l = \
                    1./(par['xi_L']*(par['g_L']-1)+1) * proj_h**cau.dev + \
                    1./par['g_L'] * perp_h**cau.dev
            stress_x = par['xi_C']*(1-par['g_C'])/par['g_L'] * perp_h**cau.dev
            stress_equiv = n_norm(
                par['p'],
                par['eq_stress'](stress_l),
                par['eq_stress'](stress_x))
            # Truncation for reverse loading
            trunc_p = 2*par['f+']*positive_part(par['h']**cau.dev)
            trunc_m = 2*par['f-']*negative_part(par['h']**cau.dev)
            return n_norm(par['q'], stress_equiv, trunc_p, trunc_m) / par['g_P']

        # Instance
        obj = EquivalentStress(function_, **par)
        obj.find_value = obj.find_value_homogeneous

        # Special update method
        def update_(*passed_values, **p):
            sig = passed_values[0]
            eps = passed_values[1]
            deps = passed_values[2]
            # Things will mess up if the stress is zero
            if sig.norm() == 0:
                return p
            # innit h if necesasary
            if p['h'].norm() < 1e-12:
                p['h'] = sig.dev.normed
                p['h^p'] = sig.dev.normed
                p['h^s'] = sig.dev.normed
            # Otherwise let's get busy!
            s_hat = sig.dev.normed
            cos_chi = s_hat**p['h']
            cos_chi_p = s_hat**p['h^p']
            cos_chi_s = s_hat**p['h^s']
            lamda = np.sign(cos_chi)
            lamda_p = np.sign(cos_chi_p)
            lamda_s = np.sign(cos_chi_s)
            # Increments!
            #-----------------------------------------------------------------
            # Microstructure deviator ----------------------------------------
            p['h'] += deps * lamda*p['k'] * (s_hat - cos_chi*p['h'])\
                    * (abs(cos_chi)**(2*p['xi_R']) + abs(cos_chi_p)**(2*p['xi_R']))
            p['h^p'] += deps * lamda_p*p['k^p'] * (s_hat - cos_chi_p*p['h^p'])
            p['h'] = p['h'].normed
            p['h^p'] = p['h^p'].normed
            # Bauschinger ----------------------------------------------------
            h_ratio = 1. if p['MULTI'] else p['hardrule'](0)/p['hardrule'](eps)
            p['g+'] += deps*(1+lamda)/2 * (p['k_1']*(1-p['g+'])/p['g+']**p['xi_B^p'])\
                     + deps*(1-lamda)/2 * (p['k_1']*(1-p['g+'])/p['g+']**p['xi_B^p'])\
                                        * (1-abs(cos_chi)**(2*p['xi_B']))\
                     + deps*(1-lamda)/2 * p['k_2']*(p['k_3']*h_ratio-p['g+'])\
                                        * abs(cos_chi)**(2*p['xi_B'])
            p['g-'] += deps*(1-lamda)/2 * (p['k_1']*(1-p['g-'])/p['g-']**p['xi_B^p'])\
                     + deps*(1+lamda)/2 * (p['k_1']*(1-p['g-'])/p['g-']**p['xi_B^p'])\
                                        * (1-abs(cos_chi)**(2*p['xi_B']))\
                     + deps*(1+lamda)/2 * p['k_2']*(p['k_3']*h_ratio-p['g-'])\
                                        * abs(cos_chi)**(2*p['xi_B'])
            p['g+'] = 1 if p['g+'] > 1 else p['g+']
            p['g-'] = 1 if p['g-'] > 1 else p['g-']
            p['g+'] = 1e-10 if p['g+'] <= 0 else p['g+']
            p['g-'] = 1e-10 if p['g-'] <= 0 else p['g-']
            p['f+'] = np.sqrt(3./8) * (1/p['g+']**p['q'] - 1)**(1./p['q'])
            p['f-'] = np.sqrt(3./8) * (1/p['g-']**p['q'] - 1)**(1./p['q'])
            # Permanent softening --------------------------------------------
            if lamda != p['g_4']:
                p['g_P^s'] = p['g_P']
                p['g_3^s'] = p['g_3']
                p['h_s'] = p['h']
            p['g_S'] += deps * -p['k_S']*p['g_S'] * (1-np.abs(lamda_s))
            p['g_3^p'] = p['g_3'] + deps * p['k_5']*p['g_S'] * (p['k_4']-p['g_3'])
            p['g_4'] = lamda
            #---
            p['g_P^p'] = p['g_P^s'] - (p['g_P^s']-p['g_3^s'])*abs(cos_chi_s)**(2*p['xi_S'])
            if p['g_P^p'] < p['g_P']:
                p['g_P'] = p['g_P^p']
            if p['g_3^p'] <= p['g_P']:
                p['g_3'] = p['g_3^p']
            else:
                p['g_3'] = p['g_P']
            # cross loading contraction --------------------------------------
            if abs(cos_chi) >= .996:
                p['g_C'] += deps * p['k_C'] * (p['C']-p['g_C'])
            else:
                p['g_C'] += deps * p['k_C^p'] * (1-p['g_C']) / p['g_C']**p['xi_C^p']
            # latent hardening -----------------------------------------------
            h_ratio = 1. if p['MULTI'] else p['hardrule'](0)/p['hardrule'](eps)
            p['g_L'] += deps * p['k_L'] *(\
                1 - p['g_L'] + (1-h_ratio)*(\
                    np.sqrt(p['L'] + (1-p['L'])*abs(cos_chi_p)**(2*p['xi_L^p']))\
                    -1\
                )\
            )
            #-----------------------------------------------------------------
            return p
        obj.updated_variables = update_
        return obj

    @staticmethod
    def francois2001(**par):
        """Francois' egg-shaped yield function [2]

        f_fra01  = | S_d - X |

        where X is the backstress and S_d a modified deviator:

        S_d = S + (S_0:S_0) X/|X| / K**2
        S_0 = S - (S:X)X/|X|

        References
        ----------
        .. [2] Francois, 2001 : "A plasticity model with yield surface distortion
            for non proportional loading".
            International Journal of Plasticity.
        """
        # The default equivalent stress
        par.setdefault('eq_stress', EquivalentStress.mises())
        # Microstructure deviator
        par.setdefault('X', Tensor([0, 0, 0, 0, 0, 0]))
        par.setdefault('p0', 2.)
        par.setdefault('pX', 2.)
        par.setdefault('K0', 0.)
        par.setdefault('KX', 0.)
        def function_(cau, **par):
            # Identity projector
            iden_4 = Tensor.projector('iden', 3, order=4)
            # Parallel projector
            proj_x = par['X'] % par['X']
            proj_x = proj_x.normed
            # Perpendicular projector
            perp_x = iden_4 - proj_x
            # Weighted stresses
            s_x = cau.dev**proj_x
            s_0 = cau.dev**perp_x
            s_d = cau.dev + par['K0']*par['X']*(s_0.norm()**par['p0'])
            s_d = s_d + par['KX']*par['X']*(s_x.norm()**par['pX'])
            return s_d.norm()
        # Instance
        obj = EquivalentStress(function_, **par)
        obj.find_value = obj.find_value_homogeneous
        return obj


################################################################################


    def find_value(self, value, direction):
        """Return the factor r such that self(r*direction) = value

        This is a Newton method, general but far from optimal
        and very slow.
        """
        # Define residual
        find_root = lambda x: (self(x*direction) - value)
        # Its derivative
        find_root_der = lambda x: self.derivative(x*direction)**direction
        # Solve with Newton-like algorithm
        result = root_scalar(find_root, fprime=find_root_der, x0=1, method='newton',maxiter=10).root
        if np.isnan(result):
            raise Exception('Could not find root.')
        return result

    def find_value_homogeneous(self, value, direction):
        """Return the factor r such that self(r*direction) = value
        ONLY for homogeneous functions !!"""
        return value/self(direction)

    def find_value_new(self, value, direction):
        """ some smart idea?"""
        raise NotImplementedError()

    def polar_curve(self, value, nb_points=100):
        """Return polar coordinates of the equivalent scalar curve in the deviatoric plane

        That is r such that
            eq_stress(r*N) = value
        with N a normed tensorial direction equivalent to ang in the pi-plane

        Returns
        -------
        radius : array-like
        angle : array like
        """
        res = np.zeros((nb_points, 2))
        line = -1
        for ang, ten  in Tensor.gen_deviatoric_unit_circle(nb_points):
            line += 1
            res[line, 1] = ang
            res[line, 0] = self.find_value(value, ten.dev.normed)/np.sqrt(2/3)
        return res[:, 0], res[:, 1] # i.e. radius, angle

    def planar_curve(self, value, components='1122', nb_points=100):
        """Return cartesian coordinates of the eq_scalar curve is 2d planar space."""
        res = np.zeros((nb_points, 2))
        line = -1
        for ang, ten  in Tensor.gen_planar_unit_circle(nb_points, components):
            line += 1
            coeff = self.find_value(value, ten)
            res[line, 0] = np.cos(ang) * coeff
            res[line, 1] = np.sin(ang) * coeff
        return res[:, 0], res[:, 1] # i.e. x, y

    def planar_curve_1122(self, value, alt_12=0., nb_points=100):
        """Return cartesian coordinates of the eq_scalar curve is 2d planar space."""
        res = np.zeros((nb_points, 2))
        line = -1
        for ang, ten  in Tensor.gen_planar_unit_circle_1122(nb_points, alt_12):
            line += 1
            coeff = self.find_value(value, ten)
            res[line, 0] = np.cos(ang) * coeff
            res[line, 1] = np.sin(ang) * coeff
        return res[:, 0], res[:, 1] # i.e. x, y

    def polar_curve_homogeneous(self, value, nb_points=100):
        """Return polar coordinates of the equivalent scalar curve in the deviatoric plane

        Faster but ONLY fror homogeneous functions!
        """
        res = np.array([
            (value/self(ten), ang)
            for ang, ten  in Tensor.gen_deviatoric_unit_circle(nb_points)
            ])
        return res[:, 0], res[:, 1] # i.e. radius, angle


################################################################################

def n_norm(order, *args):
    """Return the nth root of the sum of the nth powers"""
    return np.sum(np.abs(np.array(args))**order)**(1./order)

def positive_part(val):
    """Return the value if positive, zero else"""
    return val if val > 0 else 0.

def negative_part(val):
    """Return the value if negative, zero else"""
    return val if val < 0. else 0.


if __name__ == "__main__":
    import doctest
    doctest.testmod()
