"""This module provide graphical support for the rest of the library

The idea is, given a context (a Canvas) to associate each object
with a graphical representation using the PLOTLY/DASH framework.

"""
import random
import numpy as np
#import dash
#import dash_core_components as dcc
#import dash_html_components as html
#from dash.dependencies import Input, Output

import plotly.graph_objects as go
#import plotly.express as px

from somech.tensors import Tensor
from somech.materials_equivalent_stresses import EquivalentStress
ED2P = Tensor.eigendeviator2polar

class Graph:
    """A class to simplify the visualization of `somech` objects

    Attributes
    ----------
    layout: dict
        This is a parameter to pass to `go.Figure.update_layout`.
        It applies an appropriate layout to the figure object.
    """
    def __init__(self, typename: str):
        """Initialize object depending on its type.

        The given string is striped of dashes, spaces and capitals.
        Then it is compared to several keywords to determine the type.
        - 'piplane' or 'deviatoricplane':
            Polar deviatoric graph
        - '3dplanestate' or 'planestate3d':
            Cartesian 3d (11, 22, 12) components of a tensor.
        - '2dplanestate' or 'planestate2d'
            Cartesian 2d combination of the above.
            This must be specified as `option` when using method `trace`
        - 'eigenspace'
            Cartesian 3D space of eigenvalues for 2nd order tensors.
        - 'physical'
            Cartesian (x1, x2, x3) for vectors and scalars

        """
        # Ensure the argument type
        if not isinstance(typename, str):
            raise TypeError('Expecting a string for argument `name`.')
        # This function will check if the provided name starts like our keywords
        name_test = lambda KW: KW.startswith(conc_str.lower())
        # Strip off spaces and dashes
        conc_str = typename.replace(' ', '')
        conc_str = conc_str.replace('-', '')
        conc_str = conc_str.replace('_', '')

        # If it matches more then once we will throw an exception
        matched = 0
        if name_test('piplane') or name_test('deviatoricplane'):
            self.canvas = CanvasDev()
            self.layout = dict(polar=dict(
                angularaxis=dict(
                    tickvals=[330, 90, 210],
                    ticktext=['S1', 'S2', 'S3'],
                )
            ))
            matched += 1
        if name_test('2dplanestate') or name_test('planestate2d'):
            self.canvas = CanvasPlane2D()
            matched += 1
        if name_test('planexy') or name_test('xy'):
            self.canvas = CanvasXY()
            matched += 1
        #if name_test('3dplanestate') or name_test('planestate3d'):
        #    self.canvas = 'p3d'
        #    matched += 1
        #if name_test('eigenspace'):
        #    self.canvas = 'eig'
        #    matched += 1
        #if name_test('physical'):
        #    self.canvas = 'phy'
        #    matched += 1
        # Sanctions if wrong name!
        if matched == 0:
            raise ValueError('Unsupported name.')
        if matched > 1:
            raise ValueError('Ambiguous name: it matched '+str(matched)+' times.')


    def trace(self, obj=None, options=None, **kwargs):
        """Return a Plotly-trace instance representing the object

        Arguments
        ---------
        obj : depends
            Something compatible with the graph type: a Tensor, an EquivalentStress
        options : depends
            Something that the function can handle, see case-wise.
        kwargs : key=value arguments
            These are directly passed to the trace instance, so whatever makes sense...
        """
        if options is None:
            options = dict()
        # Call the appropriate trace depending on the object type
        if isinstance(obj, Tensor):
            trace = self.canvas.trace_tensor(obj, options)
        elif isinstance(obj, EquivalentStress):
            trace = self.canvas.trace_equiv_stress(obj, options)
        else:
            raise Exception('This object type poses a problem')
        # Update it with added options
        trace.update(**kwargs)
        return trace


    def tikz_export(self, obj, **keyval):
        """Export an object instance to a tikz graphic (through stdout or file)

        Attributes
        ----------
        obj : the object instance
            - 'piplane': `so.Tensor`, `so.EquivalentStress`
            - '3dplanestate': NOT IMPLEMENTED
            - '2dplanestate': NOT IMPLEMENTED
            - 'eigenspace': NOT IMPLEMENTED
            - 'physical': NOT IMPLEMENTED
        **keyval : dict
            Keyword arguments.
            - 'options' : dict
                depends on the object
            - 'file_name' : str or None
                File to write or stdout if None
        """
        keyval.setdefault('options', dict())
        keyval.setdefault('file_name', None)

        # Call the appropriate trace depending on the object type
        tex_depends = ''
        if isinstance(obj, Tensor):
            tex_depends += self.canvas.tikz_tensor(obj, keyval['options'])
        elif isinstance(obj, EquivalentStress):
            tex_depends += self.canvas.tikz_equiv_stress(obj, keyval['options'])
        else:
            raise Exception('This object type poses a problem')
        the_text = TEX_PREAMBLE + tex_depends + TEX_POSTAMBLE
        if keyval['file_name']:
            with open(keyval['file_name'], 'w') as the_file:
                the_file.write(the_text)
        else:
            print(the_text)

# --------------------------------------------------------------------
# CANVAS-DEVIATORIC

class CanvasDev():
    """Canvas object for the composition of the `so.Graph` class.

    The pi-plane is described with polar coordinates (r, theta)
    where the origin of theta is aligned with the axis
                  s s_2
                  s theta= pi/2
                  s
                  s
                  s
        --------- o --------- theta=0
               _s   s_
             _s       s_
           _s           s_   theta=-pi/6
     s_3 _s               s_ s_1
        s                   s
    """
    @staticmethod
    def trace_tensor(obj, options):
        """Return a trace instance of a tensor in the pi-plane.

        Attributes
        ----------
        obj : 'so.Tensor'
        options: dict
            'type' : 'line' for a line from the origin to the point,
                point otherwise.
        """
        if obj.ord == 2:
            options.setdefault('type', 'line')
            rad, ang = ED2P(obj.dev.decomposition_eigen()[0])
            ang = ang/np.pi*180
            if options['type'] == 'line':
                rad = [0, rad]
                ang = [ang]*2
            else:
                rad = [rad]
                ang = [ang]
            return go.Scatterpolar(
                r=rad,
                theta=ang,
                hovertext='TENSOR',#str(obj),
                )
        raise NotImplementedError(
            'Wrong order'
            )

    @staticmethod
    def trace_equiv_stress(obj, options):
        """Return a trace instance of an equivalent stress in the pi-plane.
        That is a closed curve, an isoline, the function sigma = value.

        Attributes
        ----------
        obj : 'so.EquivalentStress'
        options: dict
            'value' : float
                the value (1 per default) of the isoline to draw
        """
        options.setdefault('value', 1.)
        rad, ang = obj.polar_curve(options['value'], 200)
        fig = go.Scatterpolar(
            name='equivalent stress',
            r=rad,
            theta=ang/np.pi*180,
            mode='lines',
            )
        return fig

    @staticmethod
    def trace_equiv_stress_surf(obj, opt):
        """Contour plot of the yield fun"""
        opt.setdefault('range', [-1., 1.])
        opt.setdefault('density', 100)
        dir_x = Tensor.angle2deviator(0)
        dir_y = Tensor.angle2deviator(np.pi/2)
        x_arr = np.linspace(opt['range'][0],opt['range'][1], opt['density'])
        y_arr = np.linspace(opt['range'][0],opt['range'][1], opt['density'])
        res = np.zeros([len(x_arr), len(y_arr)])
        for idx, xval in enumerate(x_arr):
            for idy, yval in enumerate(y_arr):
                res[idy,idx] = obj(xval*dir_x + yval*dir_y)
        # Valid color strings are CSS colors, rgb or hex strings
        colorscale = [[0.0, 'red'],
                      [0.5, 'white'],
                      [1.0, 'black'],
                      ]
        return go.Contour(
                z=res,
                x=x_arr,
                y=y_arr,
                connectgaps=True,
                line_smoothing=0,
                colorscale=colorscale,
                contours=dict(
                    #coloring ='heatmap',
                    showlabels = True, # show labels on contours
                    labelfont = dict( # label font properties
                        size = 10,
                        color = 'white',
                       )
                    )
                )

    @staticmethod
    def tikz_tensor(obj, options):
        """Output (stdout or file) a tikz drawing of a tensor in the pi-plane.

        Attributes
        ----------
        obj : 'so.Tensor'
        options: dict
            'style' : string
                the object style to paste in pgfplotsset
        """
        options.setdefault('style', 'thick, blue')
        if not obj.ord == 2:
            raise NotImplementedError('Wrong order')
        rad, ang = ED2P(obj.decomposition_eigen()[0])
        ang = ang/np.pi*180
        style_name = "ten_{}".format(rnd_letters())
        my_str = " \\pgfplotsset{{{0}/.style={{ {1} }} }}\n".format(style_name, options['style'])
        my_str += " \\addplot[{0}] coordinates {{(0,0) ({1:0.3f}, {2:0.3f})}};".format(
            style_name, ang, rad
            )
        return PIPLANE_PREAMBLE+my_str+PIPLANE_POSTAMBLE

    @staticmethod
    def tikz_equiv_stress(obj, options):
        """Output (stdout or file) a tikz drawing of an equivalent stress in the pi-plane.
        That is a closed curve, an isoline, the function sigma = value.

        Attributes
        ----------
        obj : 'so.EquivalentStress'
        options: dict
            'value' : float
                the value (1 per default) of the isoline to draw
            'amount' : int
                The number of function evaluations (100 per default)
            'style' : string
                the object style to paste in pgfplotsset
        """
        options.setdefault('value', 1.)
        options.setdefault('amount', 100)
        options.setdefault('style', '')#'thick, red, only marks')
        rad_list, ang_list = obj.polar_curve(options['value'], options['amount'])
        ang_list = ang_list/np.pi*180
        style_name = "eqs"
        my_str = ""
        if options['style'] != '':
            style_name += "_{}".format(rnd_letters())
            my_str += " \\pgfplotsset{{{0}/.style={{{1}}}}}\n".format(style_name, options['style'])
        # Write down addplot coords
        my_str += " \\addplot[{0}] coordinates {{ ".format(style_name)
        for i_row, ang in enumerate(ang_list):
            rad = rad_list[i_row]
            my_str += "({0:0.3f}, {1:0.3f}) ".format(ang, rad)
        return PIPLANE_PREAMBLE+my_str+r" };"+PIPLANE_POSTAMBLE
# --------------------------------------------------------------------
# CANVAS-2dPlane

class CanvasPlane2D():
    """Canvas object for the composition of the `so.Graph` class.

    Plane 2d looks at two components out of the three of a plane Tensor,
    that is two from '11', '12', '22'.
    """
    # pylint: disable=no-self-use
    def trace_tensor(self, obj, options):
        """Return a trace instance of a tensor in 2D plane state.

        Arguments
        ---------
        obj : 'so.Tensor'
        options: dict
            'components': str
                Which components to plot.
                The concatenation of two different strings.
                picked from '11', '22', '12'.
            'type' : 'line' for a line from the origin to the point,
                point otherwise.
            'origin' : 'so.Tensor'
                if specified then tensor is plotted from there
        """
        options.setdefault('origin', obj*0.)
        options.setdefault('components', '1122')
        options.setdefault('type', 'line')

        components = options['components']

        tup_x = (int(components[0])-1, int(components[1])-1)
        tup_y = (int(components[2])-1, int(components[3])-1)

        if not obj.ord == 2:
            raise Exception('Wrong order to trace Tensor in PiPLane')

        if options['type'] == 'line':
            org = options['origin']
            orig_x = org.in_vect_basis().values[tup_x]
            orig_y = org.in_vect_basis().values[tup_y]
            data_x = [orig_x, (org+obj).in_vect_basis().values[tup_x]]
            data_y = [orig_y, (org+obj).in_vect_basis().values[tup_y]]
        else:
            data_x = [obj.in_vect_basis().values[tup_x]]
            data_y = [obj.in_vect_basis().values[tup_y]]

        return go.Scatter(x=data_x, y=data_y, hovertext='TENSOR')

    def trace_equiv_stress(self, obj, options):
        """Return a trace instance of an equivalent stress in 2D plane state.
        That is a closed curve, an isoline, the function sigma = value.
        The plane is 11-22, and an altitude (12) can be specified in option.

        Arguments
        ----------
        obj : 'so.EquivalentStress'
        options: dict
            'value' : float
                the value (1 per default) of the isoline to draw
            'alt_12': integer
                the altitude in the 12 direction
        """
        options.setdefault('nb_points', 100)
        options.setdefault('value', 1.)
        options.setdefault('alt_12', 0.)
        absi, ordo = obj.planar_curve_1122(
                options['value'],
                options['alt_12'],
                options['nb_points'],
                )
        fig = go.Scatter(
            name='equivalent stress',
            x=absi, y=ordo,
            mode='lines',
            )
        return fig


# --------------------------------------------------------------------
# CANVAS-

class CanvasXY():
    """Canvas object for the composition of the `so.Graph` class.

    XY lets us plot 2D vectors and bodies
    """

    def trace_tensor(self, conf, options):
        """Return a trace instance of a tensor in 2D plane state.

        Arguments
        ---------
        conf : 'so.Tensor', deformation-gradient--like
        options: dict
            'components': str
                Which components to plot.
                The concatenation of two different strings.
                picked from '11', '22', '12'.
            'type' : 'line' for a line from the origin to the point,
                point otherwise.
            'origin' : 'so.Tensor'
                if specified then tensor is plotted from there
        """
        options.setdefault('color', 'gray')
        options.setdefault('name', '')

        #if not obj.ord == 2:
        #    raise Exception('Wrong order to trace Tensor in PiPLane')

        x_a = conf.values[0,0]
        x_b = conf.values[0,1]
        y_a = conf.values[1,0]
        y_b = conf.values[1,1]
        sqx, sqy =  [0., x_a, x_a+x_b, x_b, 0., x_a+x_b], [0., y_a, y_a+y_b, y_b, 0., y_a+y_b]
        return go.Scatter(x=sqx, y=sqy,
                          fill="toself", #line_width=0,
                          line_color=options['color'],
                          hovertext=options['name'],
                          name=options['name'],
                          )

#def np_print_as_tikz_coordinates(arr):
#    """TODO"""
#    if not len(arr.shape) == 2:
#        raise Exception('Refactor me plz')
#    the_str = ''
#    for row in arr:
#        the_str += '({0:0.3f},\t{1:0.3f})\n'.format(row[0], row[1])
#    return the_str

def rnd_letters(str_size=3):
    """TODO"""
    allowed_chars = 'abcdefghijklmnopqrsnuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return ''.join(random.choice(allowed_chars) for x in range(str_size))

######################################################################

TEX_PREAMBLE = r""" % arara: pdflatex: { shell: yes, synctex: yes }
\documentclass{minimal}

\usepackage{pgfplots}
\pgfplotsset{compat=1.5}
\usepgfplotslibrary{polar}

\begin{document}
"""

PIPLANE_PREAMBLE = r"""
\begin{tikzpicture}
 \begin{polaraxis}[
  xtick={30, 90, 150, 210, 270, 330},
  ytick=\empty,
  xticklabels={{}, $s_2$, {}, $s_3$, {}, $s_1$},
 ]

"""

PIPLANE_POSTAMBLE = r"""

 \end{polaraxis}
\end{tikzpicture}
"""


TEX_POSTAMBLE = r"""
\end{document}"""
