"""This module defines constitutive models in plasticity together with their solvers
"""
import os
import sys
import numpy as np
from scipy.optimize import root_scalar

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from somech import tensors as tsr #pylint: disable=wrong-import-position
from somech import materials_core as mc#pylint: disable=wrong-import-position
from somech import materials_equivalent_stresses as mes#pylint: disable=wrong-import-position
Tensor = tsr.Tensor
EquivalentStress = mes.EquivalentStress


class HardeningRule(mc.ParametricFunction):
    """Strain vs stress scalar relationships, identified experimentally

    New instances can be declared but a number of classical rules are built-in.
    See: HardeningRule.<hollomon, swift, ludwik, voce>
    The instances are callable:
        self(x) returns self.function(x, **self.parameters)

    Attributes
    ----------
    function : callable
        function(float, **parameters) -> float
    function_derivative: callable
        function(, **parameters) -> Tensor
    parameters : dict
        Some models require parameters p1, ..., pn:
        parameters = {"p1":value1, ..., "p1":valueN}

    Example
    -------
    >>> hard_rule = HardeningRule.hollomon(K=2, n=1)
    >>> hard_rule.parameters_update(K=1e3, n=.5)
    >>> hard_rule.parameters
    {'K': 1000.0, 'n': 0.5}
    >>> hard_rule(.02)
    141.4213562373095
    >>> hard_rule.inverse(hard_rule(.02))
    0.02000000000066393

    """
    def __init__(self, fun_, deriv_=None, **params):
        """Initialize parametric function

        Arguments
        ---------
        fun_ : callable(eps, **params)
            `eps` is a float.
            The scalar function returning equivalent stress from accumulated strain.
        deriv_ : callable(eps, **params)
            The derivative of the function above.
        params : dict
            Optional parameters for the functions.
        """
        # This will pass fun_ as attribute and initialize the parameters
        # using the ParametricFunction constructor
        super(HardeningRule, self).__init__(fun_, **params)
        # The added thing here is that we want to define a derivative function
        self.function_derivative = deriv_

    def derivative(self, eps):
        """Return the derivative of the equivalent stress function.

        Numerical derivative by default.
        Can be redefined to provide an analytical form.
        """
        if self.function_derivative is None:
            return self.derivative_numerical(eps)
        if callable(self.function_derivative):
            return self.function_derivative(eps, **self.parameters)
        raise TypeError('Class HardeningRule: `function_derivative` should be a callable.')

    def derivative_numerical(self, eps):
        """Return the derivative of a scalar-valued function of a second order tensor."""
        # We use an even-ised version of the function
        # to improve the behavior close to zero
        the_fun = lambda x: self(np.abs(x)) * np.sign(x)
        diff_len = 1E-6
        #eps = diff_len*10 if eps < diff_len else eps
        f_pluss = the_fun(eps + .5*diff_len)
        f_minus = the_fun(eps - .5*diff_len)
        deriv = (f_pluss - f_minus)/diff_len
        return deriv

    def inverse(self, sigma):
        """Return the evaluation of the inverse function strain = ()^-1(stress)"""
        if sigma > self(100.):
            return 100.
        find_root = lambda x: self(x) - sigma
        #import pdb; pdb.set_trace()
        eps_arg = root_scalar(find_root, method='bisect', bracket=[0, 10]).root
        #eps_arg = root_scalar(find_root, fprime=self.derivative, x0=1e-8, xtol=1e-10).root
        #eps_arg = root_scalar(find_root, bracket=[0, 5], method='brentq').root
        if np.isnan(eps_arg):
            #import pdb; pdb.set_trace()
            raise Exception('Inverse: I messed up the root finding, sorry')
        return eps_arg


    @staticmethod
    def hollomon(**p):
        r"""Return Hollomon hardening rule (K, n)
        :math:`\sigma = K\epsilon^n`
        """
        # set default parameters
        p.setdefault('K', 1.)
        p.setdefault('n', 2.)
        # Define functions
        def function_(eps, **p):
            return p['K']*eps**p['n']
        def deriv_(eps, **p):
            return p['K']*p['n']*eps**(p['n']-1.)
        # Return neat instance
        return HardeningRule(function_, deriv_, **p)

    @staticmethod
    def swift(**p):
        r"""Return Swift hardening rule (eps_0, n)
        :math:`\sigma = K(\epsilon_0+\epsilon)^n`
        """
        # set default parameters
        p.setdefault('K', 1.)
        p.setdefault('eps_0', 1.)
        p.setdefault('n', 2.)
        # Define functions
        def function_(eps, **p):
            return p['K']*(eps+p['eps_0'])**p['n']
        deriv_ = None
        # Return neat instance
        return HardeningRule(function_, deriv_, **p)

    @staticmethod
    def ludwik(**p):
        r"""Return Ludwik hardening rule (sig_y, K, n)
        :math:`\sigma = \sigma_y + K\epsilon^n`
        """
        # set default parameters
        p.setdefault('sig_y', 1.)
        p.setdefault('K', 1.)
        p.setdefault('n', 1.)
        # Define functions
        def function_(eps, **p):
            return p['sig_y'] + p['K']*eps**p['n']
        deriv_ = None
        # Return neat instance
        return HardeningRule(function_, deriv_, **p)

    @staticmethod
    def voce(**p):
        r"""Return Voce hardening rule (sig_s, sig_y, eps_0)
        :math:`\sigma = \sigma_s - (\sigma_s-\sigma_y)\exp(-\epsilon/\epsilon_0)`
        """
        # set default parameters
        p.setdefault('sig_s', 200.)
        p.setdefault('sig_y', 100.)
        p.setdefault('eps_0', 1.)
        # Define functions
        def function_(eps, **p):
            return p['sig_s']-(p['sig_s']-p['sig_y'])*np.exp(-eps/p['eps_0'])
        deriv_ = None
        # Return neat instance
        return HardeningRule(function_, deriv_, **p)

    @staticmethod
    def hocket_sherby(**p):
        r"""Return Hocket-Sherby hardening rule (sig_s, sig_y, eps_eta, eta)
        :math:`\sigma = \sigma_s - (\sigma_s-\sigma_y)\exp(-\epsilon**eta/\epsilon_eta)`
        """
        # set default parameters
        p.setdefault('sig_s', 200.)
        p.setdefault('sig_y', 100.)
        p.setdefault('eps_eta', .333)
        p.setdefault('eta', .5)
        # Define functions
        def function_(eps, **p):
            return p['sig_s']-(p['sig_s']-p['sig_y'])*np.exp(-eps**p['eta']/p['eps_eta'])
        deriv_ = None
        # Return neat instance
        return HardeningRule(function_, deriv_, **p)

class PlasticIsotropic(mc.ConstitutiveModel):
    """Classical time independent plasticity with one internal variable (accum. plast.).

    Attributes
    ----------
    hardening_rule : callable
        An equation like: float = hardening_rule(float)
        Takes a cumulative plastic strain and return a yield stress
    equivalent_stress : callable
        An equation like: float = equivalent_stress(Tensor)
        Take a stress tensor and return equivalent scalar.
    flow_rule : callable
        An equation like: Tensor = flow_rule(Tensor)
        Return the plastic flow direction.
    """
    var_types = np.dtype([
        ('stress', Tensor),
        ('strain', Tensor),
        ('strain_cumul', float)
        ])

    def __init__(self, HardRule, EquivStress, FlowRule=None):
        """Assign functions to object"""
        self.hardening_rule = HardRule
        self.equivalent_stress = EquivStress
        self.flow_rule = self.equivalent_stress.derivative if FlowRule is None else FlowRule
        # Assert everything is callable
        for cabalbubble in (self.hardening_rule, self.equivalent_stress, self.flow_rule):
            if not callable(cabalbubble):
                raise TypeError('All arguments in Plastic init should be callables')


    def solve(self, sols_old, sols_new):
        r"""Return solutions in plasticity without elasticity

        Attributes
        ----------
        sols_old: np structured array
            Solutions at previous (n) increment.
            The types are specified by the class variable `var_types`.
        sols_new: np structured array
            Solutions at current (n+1) increment.
            The types are specified by the class variable `var_types`.
            Only one variable is set, the others are yet unknown

        Comments
        --------
        /!\ WARNING /!\
        The solver works as expected for imposed STRESSES but
        NOT IMPOSED STRAINS.
        The reason is that the model is often not invertible.
        For example I can compute N(Sigma)=Sigma.dev.normed
        but there is no way to get Sigma back given N.
        ---
        So when 'strain' is imposed we suppose that
        - the norm of the given tensor is the target accumulated plastic strain
        - the direction of the given tensor is the stress direction.
        """
        ## --------------------------------------------------------------------
        ## MORE DETAILS
        ##
        ## The available equations are
        ##     (a) f = \bar\sigma(\sigma) - \sigma_R(p) =< 0
        ##     (b) dEps = dLambda N(sigma)
        ##     (c) dp = dLambda alpha(sigma)
        ##     ... and KTC dLam >=0; f.dLam = 0
        ## We impose the plastic work equivalence
        ##     (c') \dot p.\bar\sigma = \dot\epsilon:\sigma
        ## Which replaces (c)
        ## Which forces the expression
        ##     dLam alpha(sigma) = N(sigma):\sigma / \bar\sigma(\sigma)
        ## --------------------------------------------------------------------
        # Figure out the loading type,
        # "the set of things that are set":
        set_set = set(self._which_set_state(sols_new)) - {'time'}
        # Problem if more than one loading type
        if len(set_set) > 1:
            raise Exception('I dunno what but there has to be a problem')

        # Incremental strain from stress
        if set_set == {'stress'}:
            # Stress quantities
            sigma = sols_new['stress']
            sigma_equiv = self.equivalent_stress(sigma)
            # The flow is plastic if the eq. stress is bigger than the last yield stress
            if sigma_equiv > self.hardening_rule(sols_old['strain_cumul']):
                # Compute the flow direction N(\sigma)
                flow_direction = self.flow_rule(sigma)
                # Compute the accumulated plastic strain by inverting the harden. rule
                sols_new['strain_cumul'] = self.hardening_rule.inverse(sigma_equiv)
                diff_p = sols_new['strain_cumul'] - sols_old['strain_cumul']
                if diff_p == 0:
                    sols_new['strain'] = sols_old['strain']
                else:
                    # Compute the plastic strain as dEps = dp/alpha * N
                    # alpha comes from the plastic work equivalence
                    alpha = sigma**flow_direction/sigma_equiv
                    diff_lamda = diff_p / alpha
                    sols_new['strain'] = sols_old['strain'] + diff_lamda * flow_direction
            # Otherwise nothing happens
            else:
                sols_new['strain_cumul'] = sols_old['strain_cumul']
                sols_new['strain'] = sols_old['strain']
            #if (sols_new['strain']-sols_old['strain']).norm() > 0.05:
            #    import pdb; pdb.set_trace()

        # Imposed accumulated plastic strain
        elif set_set == {'strain_cumul'}:
            # in this case we impose the accumulated strain.
            diff_p = sols_new['strain_cumul'] - sols_old['strain_cumul']
            if diff_p < 0.:
                raise Exception('Wrong load: accumulated strain cannot decrease.')
            # We assume its direction gives the previous stress direction
            if sols_old['stress'].norm() < 1e-15:
                raise Exception('Wrong load: need a non zero old stress to fix direction.')
            sigma_dir = sols_old['stress']
            # Now we need to catch the stress value
            sigma_target = self.hardening_rule(sols_new['strain_cumul'])
            # So find C s. t. barSig(C*sigma_dir) = Sig_target
            ## ----- Method 1: general
            #residual = lambda coef: self.equivalent_stress(coef*sigma_dir) - sigma_target
            #search_limits = [-2*sigma_target, 2*sigma_target]
            #coef = root_scalar(residual, bracket=search_limits, method='brentq').root
            # ----- Method 2: for homogeneous functions of degree 1 only
            coef = sigma_target/self.equivalent_stress(sigma_dir)
            # -----
            sols_new['stress'] = coef*sigma_dir
            # Now compute the incremental strain knowing Sig
            diff_lambda = diff_p * sigma_target\
                        / (self.flow_rule(sols_new['stress'])**sols_new['stress'])
            diff_eps = diff_lambda * self.flow_rule(sols_new['stress'])
            sols_new['strain'] = sols_old['strain'] + diff_eps
            #import pdb; pdb.set_trace()

        # Imposed plastic strain
        # We would need N to be invertible whihc is just too much to ask.
        elif set_set == {'strain'}:
            # SPECIAL CASE: ordinarily strains can't be imposed
            # because the stress would be undetermined
            if abs((sols_new['strain'] - sols_old['strain']).norm()) < 1E-15:
                sols_new['strain'] = sols_new['stress']
                sols_new['stress'] = sols_old['strain'] * 0.
                return self.solve(sols_old, sols_new)
            raise Exception('Only zero imposed strain is supported.')
        else:
            raise Exception('Wrong load type it seems.')
        return sols_new

#class PlasticKinematic(PlasticIsotropic):
#    """Add one interna variable: the backstress"""

class PlasticDistortional(PlasticIsotropic):
    """Slightly modified isotropic plasticity: >>> see `help(PlasticIsotropic)` <<<

    These models are always isotropic as they always use the accumulated plastic strain.
    They do add internal variables stored "as parameters" in the `EquivalentStress` instance.

    This class differs from `PlasticIsotropic` only in two aspects:
    (i) The equivalent stress parameters are stored as `added_int_var`
    (ii) In the solving procedure:
        - the isotropic plasticity variables are normaly computed
        - then the auxiliary state variables are updated explicitely.
        To do so, we rely on method `EquivalentStress.updated_variables(sig, eps, deps, **param)`
        that must return the updated set of parameters.
    """
    # First off we append an additional set of state variable that are contained
    # in the EquivalentStress instance
    var_types = np.dtype([
        ('stress', Tensor),
        ('strain', Tensor),
        ('strain_cumul', float),
        ('added_int_var', dict),
        ])
    # The idea is to modify a little bit the plasticIsotropic class.
    # We don't consider a dedicated solver but rather an added updating step.
    # The internal variables added in the distortional case are still considered
    # only as parameters of the eq_stress function.
    # One might want a dedicated solving approach
    def solve(self, sols_old, sols_new):
        # Call the default solver to get solutions
        solutions = super(PlasticDistortional, self).solve(sols_old, sols_new)
        # Call the fancy variables updater
        sig = solutions['stress']
        eps_cum = solutions['strain_cumul']
        deps_cum = eps_cum - sols_old['strain_cumul']
        eps = solutions['strain']
        deps = eps - sols_old['strain']
        param = self.equivalent_stress.parameters
        newparam = self.equivalent_stress.updated_variables(
            sig, eps_cum, deps_cum,
            eps, deps,
            **param
        )
        #import pdb; pdb.set_trace()
        self.equivalent_stress.parameters_update(**newparam)
        solutions['added_int_var'] = newparam
        #for key_ in ('g+', 'g-'):
        ##for key_ in ('h', 'h^s', 'h^p', 'g_L', 'g_C',
        ##             'f+', 'f-', 'g+', 'g-', 'g_P', 'g_P^s',
        ##             'g_3', 'g_3^p', 'g_3^s', 'g_4', 'g_S'):
        #    print(key_+': '+str(self.equivalent_stress.parameters[key_]))
        return solutions


if __name__ == "__main__":
    import doctest
    doctest.testmod()
