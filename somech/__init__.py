"""This module concatenates and organizes *somech* sub-modules
"""

#pylint: disable=unused-import
from .tensors import Tensor

from .kinematics import LocalDeformation

from .materials_core import ParametricFunction
from .materials_core import ConstitutiveModel
from .materials_core import Load

from .materials_elasticity import LinearElastic

from .materials_equivalent_stresses import EquivalentStress

from .materials_plasticity import PlasticIsotropic
from .materials_plasticity import PlasticDistortional
from .materials_plasticity import HardeningRule

from .visualization import Graph
