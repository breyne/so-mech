#!/bin/sh

echo run doctests silently

python3 somech/tensors.py
python3 somech/materials_core.py
python3 somech/materials_elasticity.py
python3 somech/materials_equivalent_stresses.py
python3 somech/materials_plasticity.py
python3 somech/visualization.py

echo run pytest quietly

pytest \
 tests/tensors_tests.py \
 tests/materials_core_tests.py \
 tests/materials_elasticity_tests.py \
 tests/materials_equivalent_stresses_tests.py \
 tests/materials_plasticity_tests.py \
 tests/visualization_tests.py \
 tests/kinematics_tests.py \
